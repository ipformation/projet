<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class HolidayType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startDate', DateType::class, array('label' => 'Début du congé'))
            ->add('endDate', DateType::class, array('label' => 'Fin du congé'));

        if($options['isGranted']) {
            // Si role SUPER ADMIN (user)
            $builder ->add('validate', CheckboxType::class, array(
                'label'    => 'Validée',
                'required' => false,
            ));
        } else {
            
        }

        $builder ->add('Valider', SubmitType::class, array('attr' => array('class' => 'save btn')))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Holiday',
            'isGranted' => null,
        ));
    }
}
