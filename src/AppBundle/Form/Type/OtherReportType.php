<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;


class OtherReportType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date_supplies', DateType::class, array('label' => 'Date de l\'achat de fourniture*', 'required' => true))
            ->add('is_extra_transport', ChoiceType::class, array('label' => 'Est-ce un transport exceptionnel ou une fourniture de bureau ?*',
                'choices' => array('Transport Exceptionnel' => 0, 'Fourniture de bureau' => 1),
                'expanded' => true,
                'required' => true,
                'multiple' => false
            ))
            ->add('extra_transport_style', ChoiceType::class, array('label' => 'Moyen de transport*', 'required' => false,
                'choices'  => array(
                    'Taxi' => 'Taxi',
                    'Train' => 'Train',
                    'TER-RER' => 'TER-RER',
                    'Métro' => 'Métro',
                    'Bus' => 'Bus',
                )))
            ->add('extra_transport_price', TextType::class, array('label' => 'Prix du transport*', 'required' => false))
            ->add('other_supplies', TextType::class, array('label' => 'Désignation d\'achat de la fourniture*', 'required' => false))
            ->add('other_supplies_price', TextType::class, array('label' => 'Prix d\'achat de la fourniture*', 'required' => false))
            ->add('file', FileType::class, array(
                'data_class' => null,
                'label' => 'Facture(s) (PDF)*', 'required' => false, 'attr' => array('class' => 'save')))
            //Champ caché servant pour l'intégration des users et type de note de frais
        ;
        if($options['isGranted']) {
            $builder ->add('state', ChoiceType::class, array('label' => 'Etat',
                'choices'  => array(
                    '---' => 99,
                    'En cours' => 0,
                    'En attente' => 1,
                    'Validée' => 2,
                )));
        } else {
            $builder ->add('state', ChoiceType::class, array('label' => 'Etat',
                'choices'  => array(
                    '---' => 99,
                    'En cours' => 0,
                    'En attente' => 1,
                )));
        }
        $builder ->add('Valider', SubmitType::class, array('attr' => array('class' => 'save btn')));
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\OtherReport',
            'isGranted' => null,
        ));
    }
}
