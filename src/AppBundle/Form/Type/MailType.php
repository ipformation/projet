<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class MailType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('subject', TextType::class, array(
                'label' => 'Objet',
                'required' => false
            ))
            ->add('message', CKEditorType::class, array(
                'required' => false,
                'label' => 'Description',
                'config' => array(
                    'uiColor' => '#ffffff',
                    //...
                ),
            ))
            ->add('file', FileType::class, array(
                'data_class' => null,
                'label' => 'Piece jointe',
                'required' => false,
                'attr' => array('class' => 'save')
            ))
            ->add('Envoyer', SubmitType::class, array('attr' => array('class' => 'save btn')))
        ;
    }

}
