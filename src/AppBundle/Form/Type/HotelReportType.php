<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;


class HotelReportType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date_hotel', DateType::class, array(
                'label' => 'Date de la nuit d\'hôtel*',
                'required' => true
            ))
            ->add('where_hotel', TextType::class, array('label' => 'Lieu de couchage*'))
            ->add('nb_night_hotel', NumberType::class, array(
                'label' => 'Nombre de nuits effectuées*',
                'required' => true
            ))
            ->add('price_hotel', TextType::class, array('label' => 'Prix total du séjour*', 'required' => true))
            ->add('file', FileType::class, array(
                'data_class' => null,
                'label' => 'Facture(s) (PDF)*', 'required' => false, 'attr' => array('class' => 'save')))
            //Champ caché servant pour l'intégration des users et type de note de frais
        ;
        if($options['isGranted']) {
            $builder ->add('state', ChoiceType::class, array('label' => 'Etat',
                'choices'  => array(
                    '---' => 99,
                    'En cours' => 0,
                    'En attente' => 1,
                    'Validée' => 2,
                )));
        } else {
            $builder ->add('state', ChoiceType::class, array('label' => 'Etat',
                'choices'  => array(
                    '---' => 99,
                    'En cours' => 0,
                    'En attente' => 1,
                )));
        }
        $builder ->add('Valider', SubmitType::class, array('attr' => array('class' => 'save btn')));
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\HotelReport',
            'isGranted' => null,
        ));
    }
}
