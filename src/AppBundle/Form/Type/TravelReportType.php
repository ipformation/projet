<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;


class TravelReportType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date_travel', DateType::class, array('label' => 'Date du trajet*', 'required' => true))
            ->add('why_travel', CKEditorType::class, array(
                'required' => false,
                'label' => 'Motif du déplacement',
                'config' => array(
                    'uiColor' => '#ffffff',
                    //...
                ),
            ))
            ->add('where_travel', TextType::class, array('label' => 'Lieu de la mission'))
            ->add('km_travel', TextType::class, array('label' => 'Km parcourus*', 'required' => true))
            ->add('is_tolls_travel', ChoiceType::class, array('label' => 'Péage*',
                'choices' => array('non' => 0, 'oui' => 1),
                'expanded' => true,
                'required' => true,
                'multiple' => false
            ))
            ->add('tolls_travel', TextType::class, array('label' => 'Montant du péage', 'attr' => array('class' => 'test')))
            ->add('file', FileType::class, array(
                'data_class' => null,
                'label' => 'Facture(s) (PDF)*', 'required' => false, 'attr' => array('class' => 'save')))
            //Champ caché servant pour l'intégration des users et type de note de frais
        ;
        if($options['isGranted']) {
            $builder ->add('state', ChoiceType::class, array('label' => 'Etat',
                'choices'  => array(
                    '---' => 99,
                    'En cours' => 0,
                    'En attente' => 1,
                    'Validée' => 2,
                )));
        } else {
            $builder ->add('state', ChoiceType::class, array('label' => 'Etat',
                'choices'  => array(
                    '---' => 99,
                    'En cours' => 0,
                    'En attente' => 1,
                )));
        }
        $builder ->add('Valider', SubmitType::class, array('attr' => array('class' => 'save btn')));
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\TravelReport',
            'isGranted' => null,
        ));
    }
}
