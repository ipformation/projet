<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CatReportType extends AbstractType
{

public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'choices' => array(
                'Repas' => 'Repas',
                'Hôtel' => 'Hôtel',
                'Transport' => 'Transport',
                'Fournitures' => 'Fournitures',
            )
        ));
    }

        public function getParent()
    {
        return ChoiceType::class;
    }
}
