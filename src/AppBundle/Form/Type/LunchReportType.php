<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\CallbackTransformer;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;


class LunchReportType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */

    public function buildForm(FormBuilderInterface $builder, array $options) //TODO améliorer ex ville dans select autocomplete
    {
        $builder
            ->add('date_lunch', DateType::class, array('label' => 'Date du repas*', 'required' => true))
            ->add('nb_guest', ChoiceType::class, array('label' => 'Invité*',
                'choices'  => array(
                    '---' => 99,
                    'Oui' => true,
                    'Non' => false,
                ),'required' => true))
            ->add('type_lunch', ChoiceType::class, array('label' => 'Type de repas*',
                'choices'  => array(
                    '---' => null,
                    'Petit Déjeuner' => 'Petit Déjeuner',
                    'Déjeuner' => 'Déjeuner',
                    'Diner' => 'Diner',
                ),'required' => true))
            ->add('where_lunch', TextType::class, array('label' => 'Lieu du repas*', 'required' => true))
            ->add('tva_or_not', ChoiceType::class, array('label' => 'TVA',
                'choices'  => array(
                    '---' => 99,
                    'Oui' => true,
                    'Non' => false,
                )))
            ->add('amount_lunch', TextType::class, array('label' => 'Montant total du repas*', 'required' => true))
            ->add('file', FileType::class, array(
                'data_class' => null,
                'label' => 'Facture(s) (PDF)*', 'required' => false, 'attr' => array('class' => 'save')))
            //Champ caché servant pour l'intégration des users et type de note de frais
        ;
        if($options['isGranted']) {
            $builder ->add('state', ChoiceType::class, array('label' => 'Etat',
                'choices'  => array(
                    '---' => 99,
                    'En cours' => 0,
                    'En attente' => 1,
                    'Validée' => 2,
                )));
        } else {
            $builder ->add('state', ChoiceType::class, array('label' => 'Etat',
                'choices'  => array(
                    '---' => 99,
                    'En cours' => 0,
                    'En attente' => 1,
                )));
        }
        $builder ->add('Valider', SubmitType::class, array('attr' => array('class' => 'save btn')));
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\LunchReport',
            'isGranted' => null,
        ));
    }
}
