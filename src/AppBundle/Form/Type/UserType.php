<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $permissions = array(
            'User'        => 'ROLE_USER',
            'Admin'       => 'ROLE_ADMIN',
            'Super admin' => 'ROLE_SUPER_ADMIN'
        );

        $builder
            ->add('username')
            ->add('lastname')
            ->add('firstname')
            ->add('email')
            ->add('password',PasswordType::class, array(
            ))
            ->add('roles',ChoiceType::class,array(
                    'label'   => 'Roles',
                    'choices' => $permissions,
                    'multiple' => true,
                    'expanded' => true
                )
            )
            ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User',
        ));
    }

    public function getName()
    {
        return 'app_user';
    }

}