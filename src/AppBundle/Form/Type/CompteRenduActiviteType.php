<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;



class CompteRenduActiviteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $satisfaction = array(
            'Très satisfait'        => '4',
            'Satisfait'       => '3',
            'À revoir' => '2',
            'Insatisfaisant' => '1',
        );
        $builder
            ->add('nameClient')
            ->add('responsableClient')
            ->add('projetName')
            ->add('startDate', DateType::class)
            ->add('endDate', DateType::class)
            ->add('emailContact')
            ->add('descriptionRapport')
            ->add('nbAccidentWithStop')
            ->add('nbAccidentWithoutStop')
            ->add('nbAccidentPath')
            ->add('nbDayStopSick',NumberType::class,array(
                    'attr' => array(
                        'min' => 5,
                        'max' => 50
                    )
                )
            )
            ->add('satisfactionClient',ChoiceType::class,array(
                    'label'   => 'Satisfaction Client',
                    'choices' => $satisfaction,
                    'multiple' => false,
                    'expanded' => true
                )
            )
            ->add('satisfactionConsultant',ChoiceType::class,array(
                    'label'   => 'Satisfaction Consultant',
                    'choices' => $satisfaction,
                    'multiple' => false,
                    'expanded' => true
                )
            )
            ->add('amelioration')
            ->add('activityToDo')
            ->add('commentaireConsultant')
            ->add('place')
            ->add('dateFormation', DateType::class)
            ->add('Valider', SubmitType::class, array('attr' => array('class' => 'save btn')))

        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\CompteRenduActivite'
        ));
    }
}
