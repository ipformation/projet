<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class OfferType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array('label' => 'Titre'))
//            ->add('description')
//          TODO Choisir les options du ckeditor
            ->add('description', CKEditorType::class, array(
                    'config' => array(
                        'uiColor' => '#ffffff',
                        //...
                    ),
                ))
            ->add('available', CheckboxType::class, array(
                'label'    => 'Publiée',
                'required' => false,
            ))
            ->add('keyword', TextType::class, array('label' => 'Mots Clés'))
            ->add('Valider', SubmitType::class, array('attr' => array('class' => 'save btn')));
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Offer'
        ));
    }
}
