<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ArticleType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array('label' => 'Titre'))
            ->add('description', CKEditorType::class, array(
                'required' => false,
                'label' => 'Description',
                'config' => array(
                    'uiColor' => '#ffffff',
                    //...
                ),
            ))
            ->add('file', FileType::class, array(
                'data_class' => null,
                'label' => 'Image* (1440×450)',
                'required' => false,
                'attr' => array('class' => 'save')
            ))
            ->add('published', CheckboxType::class, array(
                'label'    => 'Publiée',
                'required' => false,
            ))
            ->add('datePublish', DateType::class, array('label' => 'Date de publication'))
            ->add('Valider', SubmitType::class, array('attr' => array('class' => 'save btn')))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Article'
        ));
    }
}
