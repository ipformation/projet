<?php

namespace AppBundle\Twig\Extension;

/**
 * GetClassExtension class.
 *
 * @extends Twig_Extension
 */
class GetClassExtension extends \Twig_Extension
{

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            'class' => new \Twig_SimpleFunction('class', array($this, 'getClass'))
        );
    }

    /**
     * @param $object
     * @return string
     */
    public function getClass($object)
    {
        return (new \ReflectionClass($object))->getShortName();
    }

    /**
     * getName function.
     *
     * @access public
     * @return void
     */
    public function getName()
    {
        return 'get_class_extension';
    }
}