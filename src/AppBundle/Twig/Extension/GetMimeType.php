<?php

namespace AppBundle\Twig\Extension;

/**
 * GetMimeType class.
 *
 * @extends Twig_Extension
 */
class GetMimeType extends \Twig_Extension
{

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            'type' => new \Twig_SimpleFunction('type', array($this, 'getType'))
        );
    }

    /**
     * @param $object
     * @return string
     */
    public function getType($object)
    {
        return pathinfo($object, PATHINFO_EXTENSION);
    }

    /**
     * getName function.
     *
     * @access public
     * @return void
     */
    public function getName()
    {
        return 'get_type_extension';
    }
}