<?php

namespace AppBundle\Util;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{


    public function upload(UploadedFile $file, $uploadDir)
    {
        $fileName = md5(uniqid()).'.'.$file->guessExtension();

        $file->move($uploadDir, $fileName);

        return $fileName;
    }
}