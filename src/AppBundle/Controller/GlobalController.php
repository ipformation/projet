<?php

namespace AppBundle\Controller;

use Ivory\CKEditorBundle\Exception\Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class GlobalController extends Controller
{

    /**
     * @Route("/global/index")
     * @Template("AppBundle::header.html.twig")
     */
    public function indexAction(Request $request)
    {

        $calendar = $this->container->get('user.google_calendar');
        $gmail = $this->container->get('user.google_gmail');
        $contact = $this->container->get('user.google_contact');
        $userCalendar = $calendar->getUserCalendar();
        $userMessage = $gmail->getUserUnreadEmail();
        $userContact = $contact->getuserContact();

        return array(
            'userCalendar' => $userCalendar,
            'userContact' => $userContact,
            'userMessage' => $userMessage,
        );
    }
}
