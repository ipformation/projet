<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Article;
use AppBundle\Form\Type\ArticleType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

/**
 * Article controller.
 *
 * @Route("/article")
 */
class ArticleController extends Controller
{
    /**
     * Lists all Article entities.
     *
     * @Route("/", name="article_index")
     * @Template
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $articles = $em->getRepository('AppBundle:Article')->findAll();

        return array(
            'articles' => $articles,
        );
    }

    /**
     * Creates a new Article entity.
     *
     * @Route("/add", name="article_add")
     * @Template
     * @Method({"GET", "POST"})
     */
    public function addAction(Request $request)
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //On recupere l'instance de l'utilisateur connecté et on la passe direct au persist
            $user_id = $this->getUser();
            $article->setUser($user_id);

            // $file stores the uploaded image file
            $file = $article->getFile();

            $path = $this->get('kernel')->getRootDir(). "/../web/uploads/image/";
            $fileName = $this->get('app.file_uploader')->upload($file,$path);

            $article->setFile($fileName);
            $article->setWebPath($path);

            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            // Pour la création auto des articles sur le front...
            //Création de la variable de l'id de l'article à passer en GET
            $articleId = $article->getId();
            //On lance le script de création d'offres d'emploi sur le front
            $url = "http://ipssi-front.com/auto-article-create.php?id=".$articleId;
            // create a new cURL resource
            $ch = curl_init();
            // set URL and other appropriate options
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            // grab URL and pass it to the browser
            curl_exec($ch);
            // close cURL resource, and free up system resources
            curl_close($ch);

            $this->addFlash(
                'success',
                'Article bien ajouté.'
            );

            return $this->redirectToRoute('article_index');

        }

        return array(
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Article entity.
     *
     * @Route("/{id}", name="article_show")
     * @Template
     * @Method("GET")
     */
    public function showAction(Article $article)
    {
        return array(
            'article' => $article,
        );


    }

    /**
     * Displays a form to edit an existing Article entity.
     *
     * @Route("/{id}/edit", name="article_edit")
     * @Template
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();

        $article = $this->getDoctrine()
            ->getRepository('AppBundle:Article')
            ->findOneBy(array('id' => $id ));

        $editForm = $this->createForm(ArticleType::class, $article);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            if ($editForm->getData()->getFile() !== null) {

                $file = $editForm->getData()->getFile();
                $path = $this->get('kernel')->getRootDir(). "/../web/uploads/image/";
                $fileName = $this->get('app.file_uploader')->upload($file,$path);

                $article->setFile($fileName);
                $article->setWebPath("/uploads/file/".$fileName);
            }

            $em->persist($article);
            $em->flush();

            $this->addFlash(
                'success',
                'Article bien édité.'
            );

            //Modification en front
            $articleId = $article->getId();
            //On lance le script de modification d'article sur le front
            $url = "http://ipssi-front.com/auto-article-edit.php?id=".$articleId;
            // create a new cURL resource
            $ch = curl_init();
            // set URL and other appropriate options
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            // grab URL and pass it to the browser
            curl_exec($ch);
            // close cURL resource, and free up system resources
            curl_close($ch);

            return $this->redirectToRoute('article_index');
        }

        return array(
            'edit_form' => $editForm->createView(),
        );
    }

    /**
     * Deletes a Article entity.
     *
     * @Route("/delete/{id}", name="article_delete")
     */
    public function deleteAction(Request $request, Article $id)
    {

        $em = $this->getDoctrine()->getManager();

        $article = $this->getDoctrine()
            ->getRepository('AppBundle:Article')
            ->findOneBy(array('id' => $id ));

        //Suppression en front
        $articleId = $article->getId();

        //On lance le script de suppression d'offres d'emploi sur le front
        $url = "http://ipssi-front.com/auto-article-delete.php?id=".$articleId;
        // create a new cURL resource
        $ch = curl_init();
        // set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        // grab URL and pass it to the browser
        curl_exec($ch);
        // close cURL resource, and free up system resources
        curl_close($ch);

        $em->remove($article);
        $em->flush();


        $this->addFlash(
            'success',
            'L\'article a été supprimé.'
        );

        return $this->redirectToRoute('article_index');


    }

    /**
     * Get article for RESTApi.
     * @Route("/rest/article/{id}", name="app_get_article")
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function getArticleAction($id)
    {
        $article = $this->getDoctrine()
            ->getRepository('AppBundle:Article')
            ->findOneBy(array('id' => $id ));

        $obj = $article;
        $serializer = new Serializer(
            array(new GetSetMethodNormalizer()),
            array('json' => new JsonEncoder())
        );
        $json = $serializer->serialize($obj, 'json');

        $response = new \Symfony\Component\HttpFoundation\Response($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
