<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Offer;
use AppBundle\Form\Type\OfferType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
/**
 * Offer controller.
 *
 * @Route("/offer")
 */
class OfferController extends Controller
{
    /**
     * Lists all Offer entities.
     *
     * @Route("/", name="app_offer_index")
     * @Template
     */
    public function indexAction()
    {

        $offers = $this->getDoctrine()
            ->getRepository('AppBundle:Offer')
            ->findAll();

        return array(
            'offers' => $offers,
        );
    }

    /**
     * Creates a new Offer entity.
     *
     * @Route("/add", name="app_offer_add")
     * @Template
     */
    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $offer = new Offer();
        $offerForm = $this->createForm( OfferType::class, $offer);
        $offerForm->handleRequest($request);

        if ($offerForm->isValid()) {
            $em->persist($offer);
            $em->flush();

            // Pour la création auto des offres sur le front...
            //Création de la variable de l'id de l'offre à passer en GET
            $offerId = $offer->getId();
            //On lance le script de création d'offres d'emploi sur le front
            $url = "http://ipssi-front.com/auto-job-create.php?id=".$offerId;
            // create a new cURL resource
            $ch = curl_init();
            // set URL and other appropriate options
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            // grab URL and pass it to the browser
            curl_exec($ch);
            // close cURL resource, and free up system resources
            curl_close($ch);

            $this->addFlash(
                'notice',
                'L offre a été crée'
            );

            return $this->redirectToRoute('app_offer_index');

        }
        return array(
            'form' => $offerForm->createView(),
        );

    }

    /**
     * Finds and displays a Offer entity.
     *
     * @Route("/{id}", name="app_offer_show")
     * @Template
     * @Method("GET")
     */
        public function showAction(Offer $offer)
        {
            return array(
                'offer' => $offer,
            );
        }

    /**
     * Displays a form to edit an existing Offer entity.
     * @Route("/{id}/edit", name="app_offer_edit")
     * @Template
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse     *
     *
     */
    public function editAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();

        $offer = $this->getDoctrine()
            ->getRepository('AppBundle:Offer')
            ->findOneBy(array('id' => $id ));

        $offerForm = $this->createForm(OfferType::class, $offer);
        $offerForm->handleRequest($request);

        if ($offerForm->isValid()) {
            $em->persist($offer);
            $em->flush();

            //Suppression en front
            $offerId = $offer->getId();
            //On lance le script de suppression d'offres d'emploi sur le front
            $url = "http://ipssi-front.com/auto-job-edit.php?id=".$offerId;
            // create a new cURL resource
            $ch = curl_init();
            // set URL and other appropriate options
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            // grab URL and pass it to the browser
            curl_exec($ch);
            // close cURL resource, and free up system resources
            curl_close($ch);

            $this->addFlash(
                'notice',
                'L offre a été mise à jour.'
            );

            return $this->redirectToRoute('app_offer_index', array(
                'id' => $id
            ));
        }


        return array(
            'offerForm' => $offerForm->createView()
        );

    }

    /**
     * Deletes a Offer entity.
     *
     * @Route("/delete/{id}", name="app_offer_delete")
     *
     */
    public function deleteAction(Request $request, Offer $id)
    {


        $em = $this->getDoctrine()->getManager();

        $offer = $this->getDoctrine()
            ->getRepository('AppBundle:Offer')
            ->findOneBy(array('id' => $id ));

        //Suppression en front
        $offerId = $offer->getId();
        //On lance le script de suppression d'offres d'emploi sur le front
        $url = "http://ipssi-front.com/auto-job-delete.php?id=".$offerId;
        // create a new cURL resource
        $ch = curl_init();
        // set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        // grab URL and pass it to the browser
        curl_exec($ch);
        // close cURL resource, and free up system resources
        curl_close($ch);

        $em->remove($offer);
        //$em->delete($offer);
        $em->flush();

        $this->addFlash(
            'notice',
            'L offre a été supprimée.'
        );
        return $this->redirectToRoute('app_offer_index');
    }

    /**
     * Return the overall offers list.
     * Get offers for RESTApi.
     * @Route("/rest/offers", name="app_get_offers")
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function getOffersAction()
    {
        $offers = $this->getDoctrine()
            ->getRepository('AppBundle:Offer')
            ->createQueryBuilder('q')
            ->getQuery()
            ->getArrayResult();
        return new JsonResponse(array(
            'offers' => $offers,
        ));
    }

    /**
     * Get offer for RESTApi.
     * @Route("/rest/offer/{id}", name="app_get_offer")
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function getOfferAction($id)
    {
        $offer = $this->getDoctrine()
            ->getRepository('AppBundle:Offer')
            ->findOneBy(array('id' => $id ));

        $obj = $offer;
        $serializer = new Serializer(
            array(new GetSetMethodNormalizer()),
            array('json' => new JsonEncoder())
        );
        $json = $serializer->serialize($obj, 'json');

        $response = new \Symfony\Component\HttpFoundation\Response($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Export Offer
     * @Route("/export/offers/", name="app_export_offers")
     * @return Response
     */
    public function generateCsvAction()
    {

        $em = $this->getDoctrine()->getEntityManager();

        $iterableResult = $em->getRepository('AppBundle:Offer')->createQueryBuilder('a')->getQuery()->iterate();

        $handle = fopen('php://memory', 'r+');
        // Add the header of the CSV file
        fputcsv($handle, array('Id', 'Titre', 'Description', 'Disponible', 'Mots Clés'),';');

        while (false !== ($row = $iterableResult->next())) {
            $avalaible = ($row[0]->getAvailable()) ? 'oui' : 'non';
            fputcsv($handle, array('Id' => $row[0]->getId()
            , 'Titre' => $row[0]->getTitle()
            , 'Description' => strip_tags(html_entity_decode($row[0]->getDescription()))
            , 'Disponible' => $avalaible
            , 'Mots Clés' => $row[0]->getKeyword()
            ));
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        return new Response($content, 200, array(
            'Content-Type' => 'application/force-download',
            'Content-Disposition' => 'attachment; filename="export_offres.csv"'
        ));
    }

}
