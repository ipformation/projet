<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\CompteRenduActivite;
use AppBundle\Form\Type\CompteRenduActiviteType;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * CompteRenduActivite controller.
 *
 * @Route("/compterenduactivite")
 */
class CompteRenduActiviteController extends Controller
{
    /**
     * Lists all CompteRenduActivite entities.
     *
     * @Route("/", name="compterenduactivite_index")
     * @Template
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $compteRenduActivites = $em->getRepository('AppBundle:CompteRenduActivite')->findAll();

        return  array(
            'compteRenduActivites' => $compteRenduActivites,
        );
    }

    /**
     * Creates a new CompteRenduActivite entity.
     *
     * @Route("/add", name="compterenduactivite_add")
     * @Template
     * @Method({"GET", "POST"})
     */
    public function addAction(Request $request)
    {

        $compteRenduActivite = new CompteRenduActivite();

        $form = $this->createForm(CompteRenduActiviteType::class, $compteRenduActivite);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //On recupere l'instance de l'utilisateur connecté et on la passe direct au persist
            $user_id = $this->getUser();
            $compteRenduActivite->setUser($user_id);

            $em = $this->getDoctrine()->getManager();
            $em->persist($compteRenduActivite);
            $em->flush();

            $this->addFlash(
                'success',
                'Compte rendu d\'activité bien ajouté.'
            );


            return $this->redirectToRoute('compterenduactivite_index');

        }

        return  array(
            'form' => $form->createView(),
        );
    }


    /**
     * Displays a form to edit an existing CompteRenduActivite entity.
     *
     * @Route("/{id}/edit", name="compterenduactivite_edit")
     * @Method({"GET", "POST"})
     * @Template
     */
    public function editAction(Request $request,$id)
    {


        $cra = $this->getDoctrine()
            ->getRepository('AppBundle:CompteRenduActivite')
            ->findOneBy(array('id' => $id ));

        $editForm = $this->createForm(CompteRenduActiviteType::class, $cra);
        $editForm->handleRequest($request);


        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($cra);
            $em->flush();

            $this->addFlash(
                'success',
                'Compte rendu d\'activité bien edité.'
            );

            return $this->redirectToRoute('compterenduactivite_index');
        }

        return array(
            'edit_form' => $editForm->createView(),
        );

    }

    /**
     * Deletes a CompteRenduActivite entity.
     *
     * @Route("/{id}", name="compterenduactivite_delete")
     */
    public function deleteAction(Request $request, CompteRenduActivite $id)
    {

        $em = $this->getDoctrine()->getManager();

        $cra = $this->getDoctrine()
            ->getRepository('AppBundle:CompteRenduActivite')
            ->findOneBy(array('id' => $id ));


        $em->remove($cra);
        $em->flush();

        $this->addFlash(
            'notice',
            'Le compte rendu d\'activité a été supprimé.'
        );
        return $this->redirectToRoute('compterenduactivite_index');


    }

    /**
     * Export Liste CRA
     * @Route("/export/cra/", name="app_cra_export")
     * @return Response
     */
    public function generateCsvAction()
    {

        $em = $this->getDoctrine()->getEntityManager();

        $iterableResult = $em->getRepository('AppBundle:CompteRenduActivite')->createQueryBuilder('a')->getQuery()->iterate();

        $handle = fopen('php://memory', 'r+');
        // Add the header of the CSV file
        fputcsv($handle, array('Id'
        , 'Nom du client'
        , 'Nom du consultant'
        , 'Nom du projet'
        , 'Début du projet'
        , 'Fin du projet'
        , 'Email de contact'
        , 'Description du rapport'
        , 'Nombre d\'accident avec arrêt'
        , 'Nombre d\'accident sans arrêt'
        , 'Nombre d\'accident de trajet'
        , 'Nombre de jours malade'
        , 'Indice de satisfaction client'
        , 'Indice de satisfaction consultant'
        , 'Amélioration'
        , 'Activité restante'
        , 'Commentaire du consultant'
        , 'Lieu'
        , 'Date de formation'
        , 'Statut'
        ),';');

        while (false !== ($row = $iterableResult->next())) {
            $approuved = ($row[0]->getValidate()) ? 'accepté' : 'refusé';
            fputcsv($handle, array(
            'Id' => $row[0]->getId()
            , 'nameClient' => $row[0]->getnameClient()
            , 'responsableClient' => $row[0]->getresponsableClient()
            , 'projetName' => $row[0]->getprojetName()
            , 'startDate' => $row[0]->getstartDate()->format('d/m/Y')
            , 'endDate' => $row[0]->getendDate()->format('d/m/Y')
            , 'emailContact' => $row[0]->getemailContact()
            , 'descriptionRapport' => $row[0]->getdescriptionRapport()
            , 'nbAccidentWithStop' => $row[0]->getnbAccidentWithStop()
            , 'nbAccidentWithoutStop' => $row[0]->getnbAccidentWithoutStop()
            , 'nbAccidentPath' => $row[0]->getnbAccidentPath()
            , 'nbDayStopSick' => $row[0]->getnbDayStopSick()
            , 'satisfactionClient' => $row[0]->getsatisfactionClient()
            , 'satisfactionConsultant' => $row[0]->getsatisfactionConsultant()
            , 'amelioration' => $row[0]->getamelioration()
            , 'activityToDo' => $row[0]->getactivityToDo()
            , 'commentaireConsultant' => $row[0]->getcommentaireConsultant()
            , 'place' => $row[0]->getplace()
            , 'dateFormation' => $row[0]->getdateFormation()->format('d/m/Y')
            , 'Accept' => $approuved
            ));
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        return new Response($content, 200, array(
            'Content-Type' => 'application/force-download',
            'Content-Disposition' => 'attachment; filename="export_cra.csv"'
        ));
    }


}
