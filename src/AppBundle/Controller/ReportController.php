<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;


use AppBundle\Entity\Report;
use AppBundle\Entity\LunchReport;
use AppBundle\Entity\TravelReport;
use AppBundle\Entity\HotelReport;
use AppBundle\Entity\OtherReport;

use AppBundle\Form\Type\ReportType;
use AppBundle\Form\Type\LunchReportType;
use AppBundle\Form\Type\TravelReportType;
use AppBundle\Form\Type\HotelReportType;
use AppBundle\Form\Type\OtherReportType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Tree\Fixture\Closure\User;

/**
 * Report controller.
 *
 * @Route("/report")
 */
class ReportController extends Controller
{

    /**
     * Lists all Report entities.
     *
     * @Route("/download_route/{filename}", name="download_route")
     * @Template
     */
    public function downloadAction($filename)
    {

        $path = $this->get('kernel')->getRootDir(). "/../web/uploads/file/";
        $response = new Response();
        $response->setContent(file_get_contents($path.$filename));
        $response->headers->set('Content-Type', 'application/force-download'); // modification du content-type pour forcer le téléchargement (sinon le navigateur internet essaie d'afficher le document)
        $response->headers->set('Content-disposition', 'filename='. $filename);

        return $response;
    }

    /**
     * Lists all Report entities.
     *
     * @Route("/", name="app_report_index")
     * @Template
     */
    public function indexAction()
    {
        //Liste de toutes les notes confondues
        $Reports = $this->getDoctrine()
            ->getRepository('AppBundle:Report')
            ->findAll();

        return array(
            'Reports' => $Reports
        );
    }

    /**
     * Deletes a Report entity.
     *
     * @Route("//delete/{id}", name="app_report_delete")
     */
    public function deleteReportAction(Request $request, Report $id)
    {

        $em = $this->getDoctrine()->getManager();

        $Report = $this->getDoctrine()
            ->getRepository('AppBundle:Report')
            ->findOneBy(array('id' => $id ));

        $em->remove($Report);
        $em->flush();

        $this->addFlash(
            'success',
            'La note de frais a été supprimée.'
        );

        return $this->redirectToRoute('app_report_index');
    }

    /**
     * Lists Hotel Report entities.
     *
     * @Route("/hotel", name="app_hotel_index")
     * @Template
     */
    public function indexHotelAction()
    {

        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            //Liste des notes hôtel

            $HotelReports = $this->getDoctrine()
                ->getRepository('AppBundle:HotelReport')
                ->findAll();
        }else{
            //Liste des notes hôtel
            $user = $this->getUser();
            $user_id = $user->getId();

            $HotelReports = $this->getDoctrine()
                ->getRepository('AppBundle:HotelReport')
                ->findBy(array('user' => $user_id, ));

        }

        return array(
            'HotelReports' => $HotelReports,
        );
    }

    /**
     * Lists Lunch Report entities.
     *
     * @Route("/lunch", name="app_lunch_index")
     * @Template
     */
    public function indexLunchAction()
    {


        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            //Liste des notes hôtel

            $LunchReports = $this->getDoctrine()
                ->getRepository('AppBundle:LunchReport')
                ->findAll();
        }else{
            //Liste des notes hôtel
            $user = $this->getUser();
            $user_id = $user->getId();

            //Liste des notes repas
            $LunchReports = $this->getDoctrine()
                ->getRepository('AppBundle:LunchReport')
                ->findBy(array('user' => $user_id, ));

        }



        return array(
            'LunchReports' => $LunchReports,
        );
    }

    /**
     * Lists Travel Report entities.
     *
     * @Route("/travel", name="app_travel_index")
     * @Template
     */
    public function indexTravelAction()
    {

        //Liste des notes transport véhiculé
        $TravelReports = $this->getDoctrine()
            ->getRepository('AppBundle:TravelReport')
            ->findAll();

        return array(
            'TravelReports' => $TravelReports,
        );
    }

    /**
     * Lists Other Report entities.
     *
     * @Route("/other", name="app_other_index")
     * @Template
     */
    public function indexOtherAction()
    {


        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            //Liste des notes hôtel

            $OtherReports = $this->getDoctrine()
                ->getRepository('AppBundle:OtherReport')
                ->findAll();
        }else{
            //Liste des notes hôtel
            $user = $this->getUser();
            $user_id = $user->getId();

            $OtherReports = $this->getDoctrine()
                ->getRepository('AppBundle:OtherReport')
                ->findBy(array('user' => $user_id, ));

        }

        return array(
            'OtherReports' => $OtherReports,
        );
    }

    /**
     * Creates a new Lunch Report entity.
     *
     * @Route("/add-lunch-report", name="app_lunchreport_add")
     * @Template
     */
    public function LunchReportAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $LunchReport = new LunchReport();

//        $LunchReportForm = $this->createForm( new LunchReportType($this->getUser(), $LunchReport));
        $LunchReportForm = $this->createForm(LunchReportType::class, $LunchReport, ['isGranted' => $this->get('security.authorization_checker')
            ->isGranted('ROLE_ADMIN')]);

        $LunchReportForm->handleRequest($request);

        if ($LunchReportForm->isSubmitted() && $LunchReportForm->isValid()) {

            //Add associative user in table Report
            $LunchReport->setUser($this->getUser());

            // $file stores the uploaded PDF file
            $file = $LunchReport->getFile();

            $path = $this->get('kernel')->getRootDir(). "/../web/uploads/file/";
            $fileName = $this->get('app.file_uploader')->upload($file,$path);
            $LunchReport->setFile($fileName);

            $em->persist($LunchReport);
            $em->flush();

            $this->addFlash(
                'success',
                'La note de repas a été crée'
            );

            return $this->redirectToRoute('app_lunch_index');

        }
        return array(
            'form' => $LunchReportForm->createView(),
        );


    }

    /**
     * Creates a new Travel Report entity.
     *
     * @Route("/add-travel-report", name="app_travelreport_add")
     * @Template
     */
    public function TravelReportAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $TravelReport = new TravelReport();
//        $TravelReportForm = $this->createForm( TravelReportType::class, $TravelReport);

        $TravelReportForm = $this->createForm(TravelReportType::class, $TravelReport, ['isGranted' => $this->get('security.authorization_checker')
            ->isGranted('ROLE_ADMIN')]);

        $TravelReportForm->handleRequest($request);

        if ($TravelReportForm->isSubmitted() && $TravelReportForm->isValid()) {

            //Add associative user in table Report
            $TravelReport->setUser($this->getUser());
            // $file stores the uploaded PDF file
            $file = $TravelReport->getFile();
            $path = $this->get('kernel')->getRootDir(). "/../web/uploads/file/";
            $fileName = $this->get('app.file_uploader')->upload($file,$path);
            $TravelReport->setFile($fileName);

            $em->persist($TravelReport);
            $em->flush();

            $this->addFlash(
                'success',
                'La note de transport a été crée'
            );

            return $this->redirectToRoute('app_travel_index');

        }
        return array(
            'form' => $TravelReportForm->createView(),
        );


    }

    /**
     * Creates a new Hotel Report entity.
     *
     * @Route("/add-hotel-report", name="app_hotelreport_add")
     * @Template
     */
    public function HotelReportAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $HotelReport = new HotelReport();

        $HotelReportForm = $this->createForm(HotelReportType::class, $HotelReport, ['isGranted' => $this->get('security.authorization_checker')
            ->isGranted('ROLE_ADMIN')]);

        $HotelReportForm->handleRequest($request);

        if ($HotelReportForm->isSubmitted() && $HotelReportForm->isValid()) {

            //Add associative user in table Report
            $HotelReport->setUser($this->getUser());
            // $file stores the uploaded PDF file
            $file = $HotelReport->getFile();
            $path = $this->get('kernel')->getRootDir(). "/../web/uploads/file/";
            $fileName = $this->get('app.file_uploader')->upload($file,$path);
            $HotelReport->setFile($fileName);

            $em->persist($HotelReport);
            $em->flush();

            $this->addFlash(
                'success',
                'La note d\'hôtel a été crée'
            );

            return $this->redirectToRoute('app_hotel_index');

        }
        return array(
            'form' => $HotelReportForm->createView(),
        );


    }

    /**
     * Creates a new Other Report entity.
     *
     * @Route("/add-other-report", name="app_otherreport_add")
     * @Template
     */
    public function OtherReportAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $OtherReport = new OtherReport();

        $OtherReportForm = $this->createForm(OtherReportType::class, $OtherReport, ['isGranted' => $this->get('security.authorization_checker')
            ->isGranted('ROLE_ADMIN')]);

        $OtherReportForm->handleRequest($request);

        if ($OtherReportForm->isSubmitted() && $OtherReportForm->isValid()) {

            //Add associative user in table Report
            $OtherReport->setUser($this->getUser());
            // $file stores the uploaded PDF file
            $file = $OtherReport->getFile();
            $path = $this->get('kernel')->getRootDir(). "/../web/uploads/file/";
            $fileName = $this->get('app.file_uploader')->upload($file,$path);
            $OtherReport->setFile($fileName);

            $em->persist($OtherReport);
            $em->flush();

            $this->addFlash(
                'success',
                'La note de fourniture a été crée'
            );

            return $this->redirectToRoute('app_other_index');

        }
        return array(
            'form' => $OtherReportForm->createView(),
        );


    }

    /**
     * Displays a form to edit an existing Lunch Report entity.
     *
     * @Route("/lunch/edit/{id}", name="app_lunch_edit")
     * @Template
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editLunchReportAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();

        $LunchReport = $this->getDoctrine()
            ->getRepository('AppBundle:LunchReport')
            ->findOneBy(array('id' => $id ));

        $LunchReportForm = $this->createForm(LunchReportType::class, $LunchReport, ['isGranted' => $this->get('security.authorization_checker')
            ->isGranted('ROLE_ADMIN')]);

        $LunchReportForm->handleRequest($request);


        if ($LunchReportForm->isSubmitted() && $LunchReportForm->isValid()) {


            //ADD ET EDIT UN PDF
            if ($LunchReportForm->getData()->getFile() !== null) {

                $file = $LunchReportForm->getData()->getFile();
                $path = $this->get('kernel')->getRootDir(). "/../web/uploads/file/";
                $fileName = $this->get('app.file_uploader')->upload($file,$path);

                $LunchReport->setFile($fileName);
                $LunchReport->setWebPath("/uploads/file/".$fileName);
            }

            $em->persist($LunchReport);
            $em->flush();

            $this->addFlash(
                'success',
                'La note de repas a été mise à jour.'
            );

            return $this->redirectToRoute('app_lunch_index');
        }


        return array(
            'form' => $LunchReportForm->createView(),
            'LunchReport' => $LunchReport,
        );

    }

    /**
     * Deletes a Lunch Report entity.
     *
     * @Route("/lunch/delete/{id}", name="app_lunch_delete")
     */
    public function deleteLunchReportAction(Request $request, Report $id)
    {

        $em = $this->getDoctrine()->getManager();

        $LunchReport = $this->getDoctrine()
            ->getRepository('AppBundle:LunchReport')
            ->findOneBy(array('id' => $id ));

        $em->remove($LunchReport);
        $em->flush();

        $this->addFlash(
            'success',
            'La note de repas a été supprimée.'
        );


        return $this->redirectToRoute('app_report_index');


    }

    /**
     * Displays a form to edit an existing Hôtel Report entity.
     *
     * @Route("/hotel/edit/{id}", name="app_hotel_edit")
     * @Template
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editHotelReportAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();

        $HotelReport = $this->getDoctrine()
            ->getRepository('AppBundle:HotelReport')
            ->findOneBy(array('id' => $id ));

        $HotelReportForm = $this->createForm(HotelReportType::class, $HotelReport, ['isGranted' => $this->get('security.authorization_checker')
            ->isGranted('ROLE_ADMIN')]);
        $HotelReportForm->handleRequest($request);

        if ($HotelReportForm->isValid()) { //TODO Creating an Uploader Service

            //ADD ET EDIT UN PDF
            if ($HotelReportForm->getData()->getFile() !== null) {

                $file = $HotelReportForm->getData()->getFile();
                $path = $this->get('kernel')->getRootDir(). "/../web/uploads/file/";
                $fileName = $this->get('app.file_uploader')->upload($file,$path);

                $HotelReport->setFile($fileName);
                $HotelReport->setWebPath("/uploads/file/".$fileName);
            }

            $em->persist($HotelReport);
            $em->flush();

            $this->addFlash(
                'success',
                'La note d\'hôtel a été mise à jour.'
            );

            return $this->redirectToRoute('app_hotel_index');
        }


        return array(
            'form' => $HotelReportForm->createView(),
            'HotelReport' => $HotelReport,
        );

    }

    /**
     * Deletes a Hotel Report entity.
     *
     * @Route("/hotel/delete/{id}", name="app_hotel_delete")
     */
    public function deleteHotelReportAction(Request $request, Report $id)
    {

        $em = $this->getDoctrine()->getManager();

        $HotelReport = $this->getDoctrine()
            ->getRepository('AppBundle:HotelReport')
            ->findOneBy(array('id' => $id ));

        $em->remove($HotelReport);
        $em->flush();

        $this->addFlash(
            'success',
            'La note d\'hôtel a été supprimée.'
        );


        return $this->redirectToRoute('app_report_index');


    }

    /**
     * Displays a form to edit an existing Travel Report entity.
     *
     * @Route("/travel/edit/{id}", name="app_travel_edit")
     * @Template
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editTravelReportAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();

        $TravelReport = $this->getDoctrine()
            ->getRepository('AppBundle:TravelReport')
            ->findOneBy(array('id' => $id ));

        $file = $TravelReport->getFile(); //TODO Creating an Uploader Service

//        $TravelReportForm = $this->createForm(TravelReportType::class, $TravelReport);
        $TravelReportForm = $this->createForm(TravelReportType::class, $TravelReport, ['isGranted' => $this->get('security.authorization_checker')
            ->isGranted('ROLE_ADMIN')]);
        $TravelReportForm->handleRequest($request);

        if ($TravelReportForm->isValid()) { //TODO Creating an Uploader Service

            ///ADD ET EDIT UN PDF
            if ($TravelReportForm->getData()->getFile() !== null) {

                $file = $TravelReportForm->getData()->getFile();
                $path = $this->get('kernel')->getRootDir(). "/../web/uploads/file/";
                $fileName = $this->get('app.file_uploader')->upload($file,$path);

                $TravelReport->setFile($fileName);
                $TravelReport->setWebPath("/uploads/file/".$fileName);
            }

            $em->persist($TravelReport);
            $em->flush();

            $this->addFlash(
                'success',
                'La note de transport a été mise à jour.'
            );

            return $this->redirectToRoute('app_travel_index');
        }


        return array(
            'form' => $TravelReportForm->createView(),
            'TravelReport' => $TravelReport,
        );

    }

    /**
     * Deletes a Travel Report entity.
     *
     * @Route("/travel/delete/{id}", name="app_travel_delete")
     */
    public function deleteTravelReportAction(Request $request, Report $id)
    {

        $em = $this->getDoctrine()->getManager();

        $TravelReport = $this->getDoctrine()
            ->getRepository('AppBundle:TravelReport')
            ->findOneBy(array('id' => $id ));

        $em->remove($TravelReport);
        $em->flush();

        $this->addFlash(
            'success',
            'La note de transport a été supprimée.'
        );


        return $this->redirectToRoute('app_report_index');


    }

    /**
     * Displays a form to edit an existing Other Report entity.
     *
     * @Route("/other/edit/{id}", name="app_other_edit")
     * @Template
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editOtherReportAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();

        $OtherReport = $this->getDoctrine()
            ->getRepository('AppBundle:OtherReport')
            ->findOneBy(array('id' => $id ));

        $OtherReportForm = $this->createForm(OtherReportType::class, $OtherReport, ['isGranted' => $this->get('security.authorization_checker')
            ->isGranted('ROLE_ADMIN')]);
        $OtherReportForm->handleRequest($request);

        if ($OtherReportForm->isValid()) {

            ///ADD ET EDIT UN PDF
            if ($OtherReportForm->getData()->getFile() !== null) {

                $file = $OtherReportForm->getData()->getFile();
                $path = $this->get('kernel')->getRootDir(). "/../web/uploads/file/";
                $fileName = $this->get('app.file_uploader')->upload($file,$path);

                $OtherReport->setFile($fileName);
                $OtherReport->setWebPath("/uploads/file/".$fileName);
            }

            $em->persist($OtherReport);
            $em->flush();

            $this->addFlash(
                'success',
                'La note de fourniture a été mise à jour.'
            );

            return $this->redirectToRoute('app_other_index');
        }


        return array(
            'form' => $OtherReportForm->createView(),
            'OtherReport' => $OtherReport,
        );

    }

    /**
     * Deletes a Other Report entity.
     *
     * @Route("/other/delete/{id}", name="app_other_delete")
     */
    public function deleteOtherReportAction(Request $request, Report $id)
    {

        $em = $this->getDoctrine()->getManager();

        $OtherReport = $this->getDoctrine()
            ->getRepository('AppBundle:OtherReport')
            ->findOneBy(array('id' => $id ));

        $em->remove($OtherReport);
        $em->flush();

        $this->addFlash(
            'success',
            'La note de fourniture a été supprimée.'
        );


        return $this->redirectToRoute('app_report_index');


    }

    /**
     * Export Note d'hotel
     * @Route("/export/hotel", name="app_reporthotel_export")
     * @return Response
     */
    public function generateCsvHotelAction()
    {

        $em = $this->getDoctrine()->getEntityManager();

        $iterableResult = $em->getRepository('AppBundle:HotelReport')->createQueryBuilder('a')->where('a.state = 2')->getQuery()->iterate();

        $handle = fopen('php://memory', 'r+');

        // Add the header of the CSV file
        fputcsv($handle, array('Id', 'Date', 'Lieu', 'Nombres de nuits', 'Prix'),';');

        while (false !== ($row = $iterableResult->next())) {
            fputcsv($handle, array(
                'Id' => $row[0]->getId(),
                'Date' => $row[0]->getDateHotel()->format('d/m/Y'),
                'Lieu' => $row[0]->getWhereHotel(),
                'Nuits' => $row[0]->getNbNightHotel(),
                'Prix' => $row[0]->getPriceHotel() . '€'
            ));
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        return new Response($content, 200, array(
            'Content-Type' => 'application/force-download',
            'Content-Disposition' => 'attachment; filename="export_ndf_hotel.csv"'
        ));
    }

    /**
     * Export Note de repas
     * @Route("/export/lunch", name="app_reportlunch_export")
     * @return Response
     */
    public function generateCsvLunchAction()
    {

        $em = $this->getDoctrine()->getEntityManager();

        $iterableResult = $em->getRepository('AppBundle:LunchReport')->createQueryBuilder('a')->where('a.state = 2')->getQuery()->iterate();

        $handle = fopen('php://memory', 'r+');

        // Add the header of the CSV file
        fputcsv($handle, array('Id', 'Date', 'Nombres d\'invités', 'Type de repas', 'Lieu du repas', 'TVA', 'Prix'),';');

        while (false !== ($row = $iterableResult->next())) {

            if ($row[0]->getTvaOrNot() == 1){
                $tva = 'oui';
            } else {
                $tva = 'non';
            }

            fputcsv($handle, array(
                'Id' => $row[0]->getId(),
                'Date' => $row[0]->getDateLunch()->format('d/m/Y'),
                'Nb invites' => $row[0]->getNbGuest(),
                'type repas' => $row[0]->getTypeLunch(),
                'lieu repas' => $row[0]->getWhereLunch(),
                'TVA' => $tva,
                'Prix' => $row[0]->getAmountLunch() . '€',
            ));
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        return new Response($content, 200, array(
            'Content-Type' => 'application/force-download',
            'Content-Disposition' => 'attachment; filename="export_ndf_repas.csv"'
        ));
    }

    /**
     * Export Note de transport
     * @Route("/export/travel", name="app_reporttravel_export")
     * @return Response
     */
    public function generateCsvTravelAction()
    {

        $em = $this->getDoctrine()->getEntityManager();

        $iterableResult = $em->getRepository('AppBundle:TravelReport')->createQueryBuilder('a')->where('a.state = 2')->getQuery()->iterate();

        $handle = fopen('php://memory', 'r+');

        // Add the header of the CSV file
        fputcsv($handle, array('Id', 'Date', 'Motif du déplacement', 'Lieu de la mission', 'Km parcourus', 'Péage', 'Montant du péage', 'Coût du trajet'),';');

        while (false !== ($row = $iterableResult->next())) {

            if ($row[0]->getIsTollsTravel() == 1){
                $peage = 'oui';
                $montantpeage = $row[0]->getTollsTravel(). '€';
            } else {
                $peage = 'non';
                $montantpeage = 0;
            }

            $motif = strip_tags($row[0]->getWhyTravel());

            fputcsv($handle, array(
                'Id' => $row[0]->getId(),
                'Date' => $row[0]->getDateTravel()->format('d/m/Y'),
                'Motif' => $motif,
                'Lieu' => $row[0]->getWhereTravel(),
                'Km' => $row[0]->getKmTravel(),
                'Peage' => $peage,
                'Montant peage' => $montantpeage,
                'Coût trajet' => ($row[0]->getKmTravel() * 0.40). '€',
            ));
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        return new Response($content, 200, array(
            'Content-Type' => 'application/force-download',
            'Content-Disposition' => 'attachment; filename="export_ndf_transport.csv"'
        ));
    }

    /**
     * Export Note de fournitures
     * @Route("/export/other", name="app_reportother_export")
     * @return Response
     */
    public function generateCsvOtherAction()
    {

        $em = $this->getDoctrine()->getEntityManager();

        $iterableResult = $em->getRepository('AppBundle:OtherReport')->createQueryBuilder('a')->where('a.state = 2')->getQuery()->iterate();

        $handle = fopen('php://memory', 'r+');

        // Add the header of the CSV file
        fputcsv($handle, array('Id', 'Date', 'Transport Exceptionnel', 'Coût', 'Fourniture', 'Coût'),';');

        while (false !== ($row = $iterableResult->next())) {

            if (!empty($row[0]->getExtraTransportStyle())){
                $transport_exept = $row[0]->getExtraTransportStyle();
                $montant_transport = $row[0]->getExtraTransportPrice(). '€';
            } else {
                $transport_exept = '';
                $montant_transport = '';
            }

            if (!empty($row[0]->getOtherSupplies())) {
                $fourniture = $row[0]->getOtherSupplies();
                $montant_fourniture = $row[0]->getOtherSuppliesPrice(). '€';
            } else {
                $fourniture = '';
                $montant_fourniture = '';
            }

            fputcsv($handle, array(
                'Id' => $row[0]->getId(),
                'Date' => $row[0]->getDateSupplies()->format('d/m/Y'),
                'Type1' => $transport_exept,
                'Coût1' => $montant_transport,
                'Type2' => $fourniture,
                'Coût2' => $montant_fourniture,
            ));
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        return new Response($content, 200, array(
            'Content-Type' => 'application/force-download',
            'Content-Disposition' => 'attachment; filename="export_ndf_fournitures.csv"'
        ));
    }
}
