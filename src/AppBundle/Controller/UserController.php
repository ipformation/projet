<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\User;
use AppBundle\Form\Type\UserType;

/**
 * Class UserController
 * @package AppBundle\Controller
 * @Route("/user")
 */
class UserController extends Controller
{
    /**
     * @Route("/list"))
     * @Template
     */
    public function indexAction()
    {

        $users = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findAll();

        return array(
            'users' => $users
        );
    }

    /**
     * @Route("/add")
     * @Template
     */
    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user = new User();
        $userForm = $this->createForm( UserType::class, $user);
        $userForm->handleRequest($request);

        if ($userForm->isValid()) {


            $password = $this->get('security.password_encoder')
                ->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
                        // 4) save the User!
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();


            $em->persist($user);
            $em->flush();

            $this->addFlash(
                'notice',
                'Votre utilisateur a été crée'
            );

            return $this->redirectToRoute('app_user_index');

        }
        return array(
            'userForm' => $userForm->createView(),
        );
    }

    /**
     * @Route("/user/edit/{id}")
     * @Template
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findOneBy(array('id' => $id ));

        $userForm = $this->createForm(UserType::class, $user);
        $userForm->handleRequest($request);

        if ($userForm->isValid()) {
            $em->persist($user);
            $em->flush();

            $this->addFlash(
                'notice',
                'Votre user a été mise à jour'
            );

            return $this->redirectToRoute('app_user_index');
        }

        return array(
            'userForm' => $userForm->createView(),
            'user' => $user,
        );
    }

    /**
     * @Route("/delete/{id}")
     * @param Request $request
     * @param $id
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findOneBy(array('id' => $id ));

        $em->delete($user);
        $em->flush();
    }

}
