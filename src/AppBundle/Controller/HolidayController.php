<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Holiday;
use AppBundle\Form\Type\HolidayType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Holiday controller.
 *
 * @Route("/holiday")
 */
class HolidayController extends Controller
{
    /**
     * Lists all Holiday entities.
     *
     * @Route("/", name="app_holiday_index")
     * @Template
     */
    public function indexAction()
    {

        $holidays = $this->getDoctrine()
            ->getRepository('AppBundle:Holiday')
            ->findAll();

        return array(
            'holidays' => $holidays
        );

    }

    /**
     * Creates a new Holiday entity.
     *
     * @Route("/add", name="app_holiday_add")
     * @Template
     */
    public function addAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $holiday = new Holiday();
//        $holidayForm = $this->createForm( HolidayType::class, $holiday);
        $holidayForm = $this->createForm( HolidayType::class, $holiday, ['isGranted' => $this->get('security.authorization_checker')
            ->isGranted('ROLE_SUPER_ADMIN')]);

        $holidayForm->handleRequest($request);

        if ($holidayForm->isValid()) {

            $em->persist($holiday);
            $em->flush();

            $this->addFlash(
                'notice',
                'La demande de congé a été faite'
            );

            return $this->redirectToRoute('app_holiday_index');

        }
        return array(
            'holidayForm' => $holidayForm->createView(),
        );


    }

//    /**
//     * Finds and displays a Holiday entity.
//     *
//     * @Route("/{id}", name="app_holiday_show")
//     * @Template
//     * @Method("GET")
//     */
//    public function showAction(Holiday $holiday)
//    {
//        return array(
//            'holiday' => $holiday,
//        );
//    }

    /**
     * Displays a form to edit an existing Holiday entity.
     *
     * @Route("/{id}/edit", name="app_holiday_edit")
     * @Template
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();

        $holiday = $this->getDoctrine()
            ->getRepository('AppBundle:Holiday')
            ->findOneBy(array('id' => $id ));

//        $holidayForm = $this->createForm(HolidayType::class, $holiday);
        $holidayForm = $this->createForm( HolidayType::class, $holiday, ['isGranted' => $this->get('security.authorization_checker')
            ->isGranted('ROLE_SUPER_ADMIN')]);
        $holidayForm->handleRequest($request);

        if ($holidayForm->isValid()) {
            $em->persist($holiday);
            $em->flush();

            $this->addFlash(
                'notice',
                'L offre a été mise à jour.'
            );

            return $this->redirectToRoute('app_holiday_index');
        }


        return array(
            'holidayForm' => $holidayForm->createView()
        );

    }

    /**
     * Deletes a Holiday entity.
     *
     * @Route("/{id}", name="app_holiday_delete")
     */
    public function deleteAction(Request $request, Holiday $id)
    {

        $em = $this->getDoctrine()->getManager();

        $holiday = $this->getDoctrine()
            ->getRepository('AppBundle:Holiday')
            ->findOneBy(array('id' => $id ));

        $em->remove($holiday);
        $em->flush();


        return $this->redirectToRoute('app_holiday_index');


    }

    /**
     * Export Liste Vacances
     * @Route("/export/holidays/", name="app_holidays_export")
     * @return Response
     */
    public function generateCsvAction()
    {

        $em = $this->getDoctrine()->getEntityManager();

        $iterableResult = $em->getRepository('AppBundle:Holiday')->createQueryBuilder('a')->getQuery()->iterate();

        $handle = fopen('php://memory', 'r+');
        // Add the header of the CSV file
        fputcsv($handle, array('Id', 'Date de début de congé', 'Date de fin de congé', 'Statut'),';');

        while (false !== ($row = $iterableResult->next())) {
            $approuved = ($row[0]->getValidate()) ? 'accepté' : 'refusé';
            fputcsv($handle, array('Id' => $row[0]->getId()
            , 'Date-in' => $row[0]->getStartDate()->format('d/m/Y')
            , 'Date-out' => $row[0]->getEndDate()->format('d/m/Y')
            , 'Accept' => $approuved
            ));
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        return new Response($content, 200, array(
            'Content-Type' => 'application/force-download',
            'Content-Disposition' => 'attachment; filename="export_conges.csv"'
        ));
    }

//    /**
//     * Creates a form to delete a Holiday entity.
//     *
//     * @param Holiday $holiday The Holiday entity
//     *
//     * @return \Symfony\Component\Form\Form The form
//     */
//    private function createDeleteForm(Holiday $holiday)
//    {
//        return $this->createFormBuilder()
//            ->setAction($this->generateUrl('holiday_delete', array('id' => $holiday->getId())))
//            ->setMethod('DELETE')
//            ->getForm()
//        ;
//    }
}
