<?php

namespace AppBundle\Controller;

use AppBundle\Form\Type\MailType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Mail controller.
 *
 * @Route("/mail")
 */
class MailController extends Controller
{
    /**
     * Lists all Mails
     *
     * @Route("/")
     * @Template
     */
    public function indexAction()
    {
        $helper = $this->container->get('user.google_gmail');
        $messages = $helper->getUserEmail();

        return array(
            'userMessage' => $messages
        );
    }

    /**
     * Show a Mail
     * @Route("/show/{email}")
     * @Template
     * @param $email
     * @return array
     */
    public function showAction($email)
    {
        $helper = $this->container->get('user.google_gmail');
        $message = $helper->getEmail($email);

        $form = $this->createForm(MailType::class);

        if ($form->isValid()) {
            $data = $form->getData();
            $helper->sendMail($data);
            $this->addFlash(
                'success',
                'Message bien envoy�.'
            );
            $this->redirectToRoute('app_mail_index');
        }

        return array(
            'userMessage' => $message,
            'form' => $form->createView()
        );
    }

}
