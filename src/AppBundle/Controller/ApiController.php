<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ApiController
 * @package AppBundle\Controller
 * @Route("/api")
 */
class ApiController extends Controller
{
    /**
     * @Route("/get-cv")
     * @Template
     */
    public function getCvAction()
    {
        $url = 'http://ipssi-front.com/wp-json/wp/v2/media/?media_type=application&search=ipssi-cv-offer';
        /* gets the CVs from a URL */
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);

        return array(
            'cvs' => json_decode($data),
        );

    }

    /**
     * @Route("/cvtheque")
     * @Template
     */
    public function cvThequeAction()
    {
        $url = 'http://ipssi-front.com/wp-json/wp/v2/media/?media_type=application&search=ipssi-cv-offer';
        /* gets the CVs from a URL */
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);

        return array(
            'cvs' => json_decode($data),
        );

    }

    /**
     * @Route("/cv/delete/{id}", name="app_api_deletecv")
     * @param $id
     */
    public function deleteCvAction($id)
    {
        $url = "http://ipssi-front.com/auto-cv-delete.php?id=".$id;
        // create a new cURL resource
        $ch = curl_init();
        // set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        // grab URL and pass it to the browser
        curl_exec($ch);
        // close cURL resource, and free up system resources
        curl_close($ch);
        return $this->redirectToRoute('app_api_getcv');
    }
}
