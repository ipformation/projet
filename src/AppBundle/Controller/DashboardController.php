<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class DashboardController extends Controller
{

    /**
     * @Route("/dashboard", name="dashboard")
     * @Template
     */
    public function indexAction(Request $request)
    {


        $session = new Session();
        $userCalendar = array();
        $userContact = array();
        $accessToken = $session->get('AccessToken');
        if (isset($accessToken)) {
            try {
                $helper = $this->container->get('user.google_client');
            }
            catch (\Google_Service_Exception $e) {
                $this->container->get('security.token_storage')->setToken(null);
                return new RedirectResponse($this->router->generate('fos_user_security_logout'));
            }
        }

        //Calcul du nombre de CV pour le dashboard
        $url = 'http://localhost/ipssi-front/wp-json/wp/v2/media';
        /* gets the CVs from a URL */
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        $countCv = (count($data));

        //Récupération des offres validées pour le dashboard
        $offers = $this->getDoctrine()
            ->getRepository('AppBundle:Offer')
            ->findBy(
                array('available' => 1)
            );

        $countOffers = count($offers);

        //Récupération des demandes de congés en attente pour le dashboard
        $holidays = $this->getDoctrine()
            ->getRepository('AppBundle:Holiday')
            ->findBy(
                array('validate' => 0)
            );
        $countHolidays = count($holidays);

        //Récupération des demandes de congés en attente pour le dashboard
        $articles = $this->getDoctrine()
            ->getRepository('AppBundle:Article')
            ->findBy(
                array('published' => 1)
            );

        return array(
            'nbCv' => $countCv,
            'offers' => $offers,
            'articles' => $articles,
            'nbOffers' => $countOffers,
            'nbHolidays' => $countHolidays,
            'userCalendar' => $userCalendar,
            'userContact' => $userContact
        );
    }

}
