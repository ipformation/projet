<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Util\Time;
/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class LunchReport extends Report
{

    public function __construct()
    {
        $this->dateLunch = new \DateTime();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     */
    private $dateLunch;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbGuest;

    /**
     * @ORM\Column(type="string")
     */
    private $typeLunch;

    /**
     * @ORM\Column(type="string")
     */
    private $whereLunch;
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $tvaOrNot;

    /**
     * @ORM\Column(type="float")
     */
    private $amountLunch;

    /**
     * Set dateLunch
     *
     * @param \DateTime $dateLunch
     *
     * @return LunchReport
     */
    public function setDateLunch($dateLunch)
    {
        $this->dateLunch = $dateLunch;

        return $this;
    }

    /**
     * Get dateLunch
     *
     * @return \DateTime
     */
    public function getDateLunch()
    {
        return $this->dateLunch;
    }

    /**
     * Set nbGuest
     *
     * @param integer $nbGuest
     *
     * @return LunchReport
     */
    public function setNbGuest($nbGuest)
    {
        $this->nbGuest = $nbGuest;

        return $this;
    }

    /**
     * Get nbGuest
     *
     * @return integer
     */
    public function getNbGuest()
    {
        return $this->nbGuest;
    }

    /**
     * Set typeLunch
     *
     * @param string $typeLunch
     *
     * @return LunchReport
     */
    public function setTypeLunch($typeLunch)
    {
        $this->typeLunch = $typeLunch;

        return $this;
    }

    /**
     * Get typeLunch
     *
     * @return string
     */
    public function getTypeLunch()
    {
        return $this->typeLunch;
    }

    /**
     * Set whereLunch
     *
     * @param string $whereLunch
     *
     * @return LunchReport
     */
    public function setWhereLunch($whereLunch)
    {
        $this->whereLunch = $whereLunch;

        return $this;
    }

    /**
     * Get whereLunch
     *
     * @return string
     */
    public function getWhereLunch()
    {
        return $this->whereLunch;
    }

    /**
     * Set tvaOrNot
     *
     * @param integer $tvaOrNot
     *
     * @return LunchReport
     */
    public function setTvaOrNot($tvaOrNot)
    {
        $this->tvaOrNot = $tvaOrNot;

        return $this;
    }

    /**
     * Get tvaOrNot
     *
     * @return integer
     */
    public function getTvaOrNot()
    {
        return $this->tvaOrNot;
    }

    /**
     * Set amountLunch
     *
     * @param float $amountLunch
     *
     * @return LunchReport
     */
    public function setAmountLunch($amountLunch)
    {
        $this->amountLunch = $amountLunch;

        return $this;
    }

    /**
     * Get amountLunch
     *
     * @return float
     */
    public function getAmountLunch()
    {
        return $this->amountLunch;
    }
}
