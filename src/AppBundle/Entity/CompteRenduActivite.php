<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Util\Time;

/**
 * CompteRenduActivite
 *
 * @ORM\Table(name="compte_rendu_activite")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class CompteRenduActivite
{
    use Time;


    public function __construct()
    {
        $this->startDate = new \DateTime();
        $this->endDate = new \DateTime();
        $this->dateFormation = new \DateTime();
    }
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="name_client", type="string", length=120)
     */
    private $nameClient;

    /**
     * @var string
     *
     * @ORM\Column(name="responsable_client", type="string", length=120)
     */
    private $responsableClient;

    /**
     * @var string
     *
     * @ORM\Column(name="projet_name", type="string", length=100)
     */
    private $projetName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime")
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime")
     */
    private $endDate;

    /**
     * @var string
     *
     * @ORM\Column(name="email_contact", type="string", length=255)
     */
    private $emailContact;

    /**
     * @var string
     *
     * @ORM\Column(name="description_rapport", type="text")
     */
    private $descriptionRapport;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_accident_with_stop", type="integer")
     */
    private $nbAccidentWithStop;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_accident_without_stop", type="integer")
     */
    private $nbAccidentWithoutStop;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_accident_path", type="integer")
     */
    private $nbAccidentPath;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_day_stop_sick", type="integer")
     */
    private $nbDayStopSick;

    /**
     * @var int
     *
     * @ORM\Column(name="satisfaction_client", type="integer")
     */
    private $satisfactionClient;

    /**
     * @var int
     *
     * @ORM\Column(name="satisfaction_consultant", type="integer")
     */
    private $satisfactionConsultant;

    /**
     * @var string
     *
     * @ORM\Column(name="amelioration", type="text")
     */
    private $amelioration;

    /**
     * @var string
     *
     * @ORM\Column(name="activity_to_do", type="text")
     */
    private $activityToDo;

    /**
     * @var string
     *
     * @ORM\Column(name="commentaire_consultant", type="text")
     */
    private $commentaireConsultant;

    /**
     * @var string
     *
     * @ORM\Column(name="place", type="string", length=255)
     */
    private $place;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_formation", type="datetime")
     */
    private $dateFormation;


    /**
     * @var \Boolean
     *
     * @ORM\Column(name="validate", type="boolean")
     */
    private $validate = 0;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameClient
     *
     * @param string $nameClient
     *
     * @return CompteRenduActivite
     */
    public function setNameClient($nameClient)
    {
        $this->nameClient = $nameClient;

        return $this;
    }

    /**
     * Get nameClient
     *
     * @return string
     */
    public function getNameClient()
    {
        return $this->nameClient;
    }

    /**
     * Set responsableClient
     *
     * @param string $responsableClient
     *
     * @return CompteRenduActivite
     */
    public function setResponsableClient($responsableClient)
    {
        $this->responsableClient = $responsableClient;

        return $this;
    }

    /**
     * Get responsableClient
     *
     * @return string
     */
    public function getResponsableClient()
    {
        return $this->responsableClient;
    }

    /**
     * Set projetName
     *
     * @param string $projetName
     *
     * @return CompteRenduActivite
     */
    public function setProjetName($projetName)
    {
        $this->projetName = $projetName;

        return $this;
    }

    /**
     * Get projetName
     *
     * @return string
     */
    public function getProjetName()
    {
        return $this->projetName;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return CompteRenduActivite
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return CompteRenduActivite
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set emailContact
     *
     * @param string $emailContact
     *
     * @return CompteRenduActivite
     */
    public function setEmailContact($emailContact)
    {
        $this->emailContact = $emailContact;

        return $this;
    }

    /**
     * Get emailContact
     *
     * @return string
     */
    public function getEmailContact()
    {
        return $this->emailContact;
    }

    /**
     * Set descriptionRapport
     *
     * @param string $descriptionRapport
     *
     * @return CompteRenduActivite
     */
    public function setDescriptionRapport($descriptionRapport)
    {
        $this->descriptionRapport = $descriptionRapport;

        return $this;
    }

    /**
     * Get descriptionRapport
     *
     * @return string
     */
    public function getDescriptionRapport()
    {
        return $this->descriptionRapport;
    }

    /**
     * Set nbAccidentWithStop
     *
     * @param integer $nbAccidentWithStop
     *
     * @return CompteRenduActivite
     */
    public function setNbAccidentWithStop($nbAccidentWithStop)
    {
        $this->nbAccidentWithStop = $nbAccidentWithStop;

        return $this;
    }

    /**
     * Get nbAccidentWithStop
     *
     * @return int
     */
    public function getNbAccidentWithStop()
    {
        return $this->nbAccidentWithStop;
    }

    /**
     * Set nbAccidentWithoutStop
     *
     * @param integer $nbAccidentWithoutStop
     *
     * @return CompteRenduActivite
     */
    public function setNbAccidentWithoutStop($nbAccidentWithoutStop)
    {
        $this->nbAccidentWithoutStop = $nbAccidentWithoutStop;

        return $this;
    }

    /**
     * Get nbAccidentWithoutStop
     *
     * @return int
     */
    public function getNbAccidentWithoutStop()
    {
        return $this->nbAccidentWithoutStop;
    }

    /**
     * Set nbAccidentPath
     *
     * @param integer $nbAccidentPath
     *
     * @return CompteRenduActivite
     */
    public function setNbAccidentPath($nbAccidentPath)
    {
        $this->nbAccidentPath = $nbAccidentPath;

        return $this;
    }

    /**
     * Get nbAccidentPath
     *
     * @return int
     */
    public function getNbAccidentPath()
    {
        return $this->nbAccidentPath;
    }

    /**
     * Set nbDayStopSick
     *
     * @param integer $nbDayStopSick
     *
     * @return CompteRenduActivite
     */
    public function setNbDayStopSick($nbDayStopSick)
    {
        $this->nbDayStopSick = $nbDayStopSick;

        return $this;
    }

    /**
     * Get nbDayStopSick
     *
     * @return int
     */
    public function getNbDayStopSick()
    {
        return $this->nbDayStopSick;
    }

    /**
     * Set satisfactionClient
     *
     * @param integer $satisfactionClient
     *
     * @return CompteRenduActivite
     */
    public function setSatisfactionClient($satisfactionClient)
    {
        $this->satisfactionClient = $satisfactionClient;

        return $this;
    }

    /**
     * Get satisfactionClient
     *
     * @return int
     */
    public function getSatisfactionClient()
    {
        return $this->satisfactionClient;
    }

    /**
     * Set satisfactionConsultant
     *
     * @param integer $satisfactionConsultant
     *
     * @return CompteRenduActivite
     */
    public function setSatisfactionConsultant($satisfactionConsultant)
    {
        $this->satisfactionConsultant = $satisfactionConsultant;

        return $this;
    }

    /**
     * Get satisfactionConsultant
     *
     * @return int
     */
    public function getSatisfactionConsultant()
    {
        return $this->satisfactionConsultant;
    }

    /**
     * Set amelioration
     *
     * @param string $amelioration
     *
     * @return CompteRenduActivite
     */
    public function setAmelioration($amelioration)
    {
        $this->amelioration = $amelioration;

        return $this;
    }

    /**
     * Get amelioration
     *
     * @return string
     */
    public function getAmelioration()
    {
        return $this->amelioration;
    }

    /**
     * Set activityToDo
     *
     * @param string $activityToDo
     *
     * @return CompteRenduActivite
     */
    public function setActivityToDo($activityToDo)
    {
        $this->activityToDo = $activityToDo;

        return $this;
    }

    /**
     * Get activityToDo
     *
     * @return string
     */
    public function getActivityToDo()
    {
        return $this->activityToDo;
    }

    /**
     * Set commentaireConsultant
     *
     * @param string $commentaireConsultant
     *
     * @return CompteRenduActivite
     */
    public function setCommentaireConsultant($commentaireConsultant)
    {
        $this->commentaireConsultant = $commentaireConsultant;

        return $this;
    }

    /**
     * Get commentaireConsultant
     *
     * @return string
     */
    public function getCommentaireConsultant()
    {
        return $this->commentaireConsultant;
    }

    /**
     * Set place
     *
     * @param string $place
     *
     * @return CompteRenduActivite
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return string
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set dateFormation
     *
     * @param \DateTime $dateFormation
     *
     * @return CompteRenduActivite
     */
    public function setDateFormation($dateFormation)
    {
        $this->dateFormation = $dateFormation;

        return $this;
    }

    /**
     * Get dateFormation
     *
     * @return \DateTime
     */
    public function getDateFormation()
    {
        return $this->dateFormation;
    }

    /**
     * Set validate
     *
     * @param boolean $validate
     *
     * @return CompteRenduActivite
     */
    public function setValidate($validate)
    {
        $this->validate = $validate;

        return $this;
    }

    /**
     * Get validate
     *
     * @return boolean
     */
    public function getValidate()
    {
        return $this->validate;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return CompteRenduActivite
     */
    public function setUser(\AppBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
