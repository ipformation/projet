<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class OtherReport extends Report
{
    public function __construct()
    {
        $this->dateSupplies = new \DateTime();
    }
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     */
    private $dateSupplies;

    /**
     * @ORM\Column(type="integer")
     */
    private $isExtraTransport;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $extraTransportStyle;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $extraTransportPrice;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $otherSupplies;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $otherSuppliesPrice;

    /**
     * Set dateSupplies
     *
     * @param \DateTime $dateSupplies
     *
     * @return OtherReport
     */
    public function setDateSupplies($dateSupplies)
    {
        $this->dateSupplies = $dateSupplies;

        return $this;
    }

    /**
     * Get dateSupplies
     *
     * @return \DateTime
     */
    public function getDateSupplies()
    {
        return $this->dateSupplies;
    }

    /**
     * Set isExtraTransport
     *
     * @param integer $isExtraTransport
     *
     * @return OtherReport
     */
    public function setIsExtraTransport($isExtraTransport)
    {
        $this->isExtraTransport = $isExtraTransport;

        return $this;
    }

    /**
     * Get isExtraTransport
     *
     * @return integer
     */
    public function getIsExtraTransport()
    {
        return $this->isExtraTransport;
    }

    /**
     * Set extraTransportStyle
     *
     * @param string $extraTransportStyle
     *
     * @return OtherReport
     */
    public function setExtraTransportStyle($extraTransportStyle)
    {
        $this->extraTransportStyle = $extraTransportStyle;

        return $this;
    }

    /**
     * Get extraTransportStyle
     *
     * @return string
     */
    public function getExtraTransportStyle()
    {
        return $this->extraTransportStyle;
    }

    /**
     * Set extraTransportPrice
     *
     * @param float $extraTransportPrice
     *
     * @return OtherReport
     */
    public function setExtraTransportPrice($extraTransportPrice)
    {
        $this->extraTransportPrice = $extraTransportPrice;

        return $this;
    }

    /**
     * Get extraTransportPrice
     *
     * @return float
     */
    public function getExtraTransportPrice()
    {
        return $this->extraTransportPrice;
    }

    /**
     * Set otherSupplies
     *
     * @param string $otherSupplies
     *
     * @return OtherReport
     */
    public function setOtherSupplies($otherSupplies)
    {
        $this->otherSupplies = $otherSupplies;

        return $this;
    }

    /**
     * Get otherSupplies
     *
     * @return string
     */
    public function getOtherSupplies()
    {
        return $this->otherSupplies;
    }

    /**
     * Set otherSuppliesPrice
     *
     * @param float $otherSuppliesPrice
     *
     * @return OtherReport
     */
    public function setOtherSuppliesPrice($otherSuppliesPrice)
    {
        $this->otherSuppliesPrice = $otherSuppliesPrice;

        return $this;
    }

    /**
     * Get otherSuppliesPrice
     *
     * @return float
     */
    public function getOtherSuppliesPrice()
    {
        return $this->otherSuppliesPrice;
    }
}
