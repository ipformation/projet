<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Util\Time;


/**
 * @ORM\Table(name="hotel_report")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class HotelReport extends Report
{

    public function __construct()
    {
        $this->dateHotel = new \DateTime();
    }
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     */
    private $dateHotel;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $whereHotel;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbNightHotel;

    /**
     * @ORM\Column(type="float")
     */
    private $priceHotel;


    /**
     * Set dateHotel
     *
     * @param \DateTime $dateHotel
     *
     * @return HotelReport
     */
    public function setDateHotel($dateHotel)
    {
        $this->dateHotel = $dateHotel;

        return $this;
    }

    /**
     * Get dateHotel
     *
     * @return \DateTime
     */
    public function getDateHotel()
    {
        return $this->dateHotel;
    }

    /**
     * Set whereHotel
     *
     * @param string $whereHotel
     *
     * @return HotelReport
     */
    public function setWhereHotel($whereHotel)
    {
        $this->whereHotel = $whereHotel;

        return $this;
    }

    /**
     * Get whereHotel
     *
     * @return string
     */
    public function getWhereHotel()
    {
        return $this->whereHotel;
    }

    /**
     * Set nbNightHotel
     *
     * @param integer $nbNightHotel
     *
     * @return HotelReport
     */
    public function setNbNightHotel($nbNightHotel)
    {
        $this->nbNightHotel = $nbNightHotel;

        return $this;
    }

    /**
     * Get nbNightHotel
     *
     * @return integer
     */
    public function getNbNightHotel()
    {
        return $this->nbNightHotel;
    }

    /**
     * Set priceHotel
     *
     * @param float $priceHotel
     *
     * @return HotelReport
     */
    public function setPriceHotel($priceHotel)
    {
        $this->priceHotel = $priceHotel;

        return $this;
    }

    /**
     * Get priceHotel
     *
     * @return float
     */
    public function getPriceHotel()
    {
        return $this->priceHotel;
    }
}
