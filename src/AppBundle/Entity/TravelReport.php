<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class TravelReport extends Report
{

    public function __construct()
    {
        $this->dateTravel = new \DateTime();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     */
    private $dateTravel;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $whyTravel;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $whereTravel;

    /**
     * @ORM\Column(type="integer")
     */
    private $kmTravel;

    /**
     * @ORM\Column(type="integer")
     */
    private $isTollsTravel;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $tollsTravel;

    /**
     * Set dateTravel
     *
     * @param \DateTime $dateTravel
     *
     * @return TravelReport
     */
    public function setDateTravel($dateTravel)
    {
        $this->dateTravel = $dateTravel;

        return $this;
    }

    /**
     * Get dateTravel
     *
     * @return \DateTime
     */
    public function getDateTravel()
    {
        return $this->dateTravel;
    }

    /**
     * Set whyTravel
     *
     * @param string $whyTravel
     *
     * @return TravelReport
     */
    public function setWhyTravel($whyTravel)
    {
        $this->whyTravel = $whyTravel;

        return $this;
    }

    /**
     * Get whyTravel
     *
     * @return string
     */
    public function getWhyTravel()
    {
        return $this->whyTravel;
    }

    /**
     * Set whereTravel
     *
     * @param string $whereTravel
     *
     * @return TravelReport
     */
    public function setWhereTravel($whereTravel)
    {
        $this->whereTravel = $whereTravel;

        return $this;
    }

    /**
     * Get whereTravel
     *
     * @return string
     */
    public function getWhereTravel()
    {
        return $this->whereTravel;
    }

    /**
     * Set kmTravel
     *
     * @param integer $kmTravel
     *
     * @return TravelReport
     */
    public function setKmTravel($kmTravel)
    {
        $this->kmTravel = $kmTravel;

        return $this;
    }

    /**
     * Get kmTravel
     *
     * @return integer
     */
    public function getKmTravel()
    {
        return $this->kmTravel;
    }

    /**
     * Set isTollsTravel
     *
     * @param integer $isTollsTravel
     *
     * @return TravelReport
     */
    public function setIsTollsTravel($isTollsTravel)
    {
        $this->isTollsTravel = $isTollsTravel;

        return $this;
    }

    /**
     * Get isTollsTravel
     *
     * @return integer
     */
    public function getIsTollsTravel()
    {
        return $this->isTollsTravel;
    }

    /**
     * Set tollsTravel
     *
     * @param float $tollsTravel
     *
     * @return TravelReport
     */
    public function setTollsTravel($tollsTravel)
    {
        $this->tollsTravel = $tollsTravel;

        return $this;
    }

    /**
     * Get tollsTravel
     *
     * @return float
     */
    public function getTollsTravel()
    {
        return $this->tollsTravel;
    }
}
