<?php

namespace UserBundle\Controller;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class LoginController extends Controller
{

    /**
     * @Route("/", name="login")
     * @Template
     */
    public function indexAction()
    {
        if ($this->getUser()) {
            if ($this->get('user.role_granted')->isGranted('ROLE_ADMIN', $this->getUser())) {
                return $this->redirectToRoute('dashboard');
            }
            else {
                $this->addFlash(
                    'error',
                    'Vous ne disposez pas des permissions nécéssaires'
                );
            }

        }
        $client = $this->container->get('user.google_client');
        $authUrl = $client->getAuthUrl();

        return array(
            'authUrl' => $authUrl
        );
    }

    /**
     * @Route("/google/login", name="google_login")
     */
    public function googleLoginAction(Request $request)
    {

        $session = new Session();

        $em = $this->getDoctrine()->getEntityManager();
        $user = new User();
        $userManager = $this->container->get('fos_user.user_manager');

        if ($request->isMethod('GET') && $request->get('code') !== null ) {
            $client = $this->container->get('user.google_client');
            $userInfo = $client->getUserInfo($request->get('code'));

            $userFound = $em->getRepository('AppBundle:User')->findOneBy(array('email' => $userInfo->email));

            $myClient = $client->getClient();
            $session->set('AccessToken', $myClient->getAccessToken());

            if ($userFound == null) {

                // Waiting for gmail group
                if (preg_match('/\@(gmail)\.com/',$userInfo->email) > 0) {
                    if ($userInfo->email == "projet.ip.dev5@gmail.com") {
                        $user->setRoles(['ROLE_ADMIN','ROLE_SUPER_ADMIN']);
                    }
                    else{
                        $user->setRoles(['ROLE_ADMIN']);
                    }
                }

                $user->setEmail($userInfo->email);
                $user->setFirstName($userInfo->familyName);
                $user->setLastName($userInfo->givenName);
                $user->setUsername($userInfo->name);
                $user->setGoogleId($userInfo->id);
                $password = bin2hex(random_bytes(5));
                $user->setPlainPassword($user->getPassword());
                $userManager->updatePassword($user);
                $user->setPlainPassword($password);
                $user->setFilePath($userInfo->picture);
                $user->setEnabled(true);
                $em->persist($user);
                $em->flush();
                $message = \Swift_Message::newInstance()
                    ->setSubject('Inscription sur IPSSI')
                    ->setFrom($this->getParameter('mailer_expeditor'))
                    ->setTo($user->getEmail())
                    ->setBody(
                        $this->renderView('UserBundle:Mail:registration-password.html.twig', array(
                            'user' => $user,
                            'password' => $password,
                        )), 'text/html'
                    );
                $this->get('mailer')->send($message);

                // set user with new user
                $token = new UsernamePasswordToken($user, null, "fos_userbundle", $user->getRoles());
                $this->get("security.token_storage")->setToken($token); // log in the user found by facebook
                // now dispatch the login event
                $event = new InteractiveLoginEvent($request, $token);
                $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
                return $this->redirectToRoute('login');

            } else {
                $user = $userFound;
                $token = new UsernamePasswordToken($user, null, "fos_userbundle", $user->getRoles());
                $this->get("security.token_storage")->setToken($token); // log in the user found by facebook

                // now dispatch the login event
                $event = new InteractiveLoginEvent($request, $token);
                $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
                return $this->redirectToRoute('login');
            }
        }
    }
}
