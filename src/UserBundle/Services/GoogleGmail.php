<?php

namespace UserBundle\Services;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session as Session;
use Symfony\Component\Routing\RouterInterface as Router;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class GoogleGmail
 *
 * @package UserBundle\Services
 */
class GoogleGmail
{

    protected $client;

    public function __construct($client)
    {
        $this->client = $client;
        return $client;
    }

    public function sendMail($data)
    {
        $client = $this->client;
        $email = new \Google_Service_Gmail($client->getClient());

        $message_object = new \Google_Service_Gmail_Message();
        $encoded_message = rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
        $message_object->setRaw($encoded_message);
        $email->users_messages->send('me', $message_object);
    }

    public function getUserEmail()
    {

        $client = $this->client;
        $messages = array();
        $email = new \Google_Service_Gmail($client->getClient());
        $user = 'me';
        $emailParam = array(
            'q' => '!is:chat',
            'maxResults' => 15
        );
        
        $results = $email->users_messages->listUsersMessages($user,$emailParam);

        foreach ($results->getMessages() as $message) {
            $date = $subject = $from = "";
            $content = $email->users_messages->get($user, $message->getId(), ['format' => 'full']);
            $messagePart = $content->getPayload();

            foreach ( $messagePart->getHeaders() as $header ) {

                switch ($header->name) {
                    case 'Date':
                        $date = $header->value;
                        break;
                    case 'From':
                        $from = $header->value;
                        break;
                    case 'Subject':
                        $subject = $header->value;
                        break;
                }
            }
            $labels = $content->getLabelIds();
            
            $messageContent = array(
                'id' => $content->getId(),
                'date' => $date,
                'subject' => $subject,
                'from' => $from,
                'content' => $content->getSnippet(),
                'isRead' => in_array('UNREAD', $labels),
                'isImportant' => in_array('IMPORTANT', $labels)
            );

            array_push($messages, $messageContent);
        }
        return $messages;
    }

    public function getEmail($id)
    {

        $client = $this->client;
        $email = new \Google_Service_Gmail($client->getClient());
        $user = 'me';

        $params = array(
            'format' => 'full'
        );

        $mods = new \Google_Service_Gmail_ModifyMessageRequest();
        $mods->setRemoveLabelIds(['UNREAD']);

        $email->users_messages->modify($user, $id, $mods);

        $result = $email->users_messages->get($user, $id, $params);


        $payload = $result->getPayload();

        $parts = $payload->getParts();
        // With no attachment, the payload might be directly in the body, encoded.
        $body = $payload->getBody();
        $body = FALSE;
        // If we didn't find a body, let's look for the parts
        if(!$body) {
            foreach ($parts  as $part) {
                if($part['parts'] && !$body) {
                    foreach ($part['parts'] as $p) {
                        if($p['parts'] && count($p['parts']) > 0){
                            foreach ($p['parts'] as $y) {
                                if(($y['mimeType'] === 'text/html') && $y['body']) {
                                    $body = $this->decodeBody($y['body']->data);
                                    break;
                                }
                            }
                        } else if(($p['mimeType'] === 'text/html') && $p['body']) {
                            $body = $this->decodeBody($p['body']->data);
                            break;
                        }
                    }
                }
                if($body) {
                    break;
                }
            }
        }
        // let's save all the images linked to the mail's body:
        if($body && count($parts) > 1){
            $images_linked = array();
            foreach ($parts  as $part) {
                if($part['filename']){
                    array_push($images_linked, $part);
                } else{
                    if($part['parts']) {
                        foreach ($part['parts'] as $p) {
                            if($p['parts'] && count($p['parts']) > 0){
                                foreach ($p['parts'] as $y) {
                                    if(($y['mimeType'] === 'text/html') && $y['body']) {
                                        array_push($images_linked, $y);
                                    }
                                }
                            } else if(($p['mimeType'] !== 'text/html') && $p['body']) {
                                array_push($images_linked, $p);
                            }
                        }
                    }
                }
            }
            // special case for the wdcid...
            preg_match_all('/wdcid(.*)"/Uims', $body, $wdmatches);
            if(count($wdmatches)) {
                $z = 0;
                foreach($wdmatches[0] as $match) {
                    $z++;
                    if($z > 9){
                        $body = str_replace($match, 'image0' . $z . '@', $body);
                    } else {
                        $body = str_replace($match, 'image00' . $z . '@', $body);
                    }
                }
            }
            preg_match_all('/src="cid:(.*)"/Uims', $body, $matches);
            if(count($matches)) {
                $search = array();
                $replace = array();
                // let's trasnform the CIDs as base64 attachements 
                foreach($matches[1] as $match) {
                    foreach($images_linked as $img_linked) {
                        foreach($img_linked['headers'] as $img_lnk) {
                            if( $img_lnk['name'] === 'Content-ID' || $img_lnk['name'] === 'Content-Id' || $img_lnk['name'] === 'X-Attachment-Id'){
                                if ($match === str_replace('>', '', str_replace('<', '', $img_lnk->value))
                                    || explode("@", $match)[0] === explode(".", $img_linked->filename)[0]
                                    || explode("@", $match)[0] === $img_linked->filename){
                                    $search = "src=\"cid:$match\"";
                                    $mimetype = $img_linked->mimeType;
                                    $attachment = $gmail->users_messages_attachments->get('me', $mlist->id, $img_linked['body']->attachmentId);
                                    $data64 = strtr($attachment->getData(), array('-' => '+', '_' => '/'));
                                    $replace = "src=\"data:" . $mimetype . ";base64," . $data64 . "\"";
                                    $body = str_replace($search, $replace, $body);
                                }
                            }
                        }
                    }
                }
            }
        }
        // If we didn't find the body in the last parts, 
        // let's loop for the first parts (text-html only)
        if(!$body) {
            foreach ($parts  as $part) {
                if($part['body'] && $part['mimeType'] === 'text/html') {
                    $body = $this->decodeBody($part['body']->data);
                    break;
                }
            }
        }
        // With no attachment, the payload might be directly in the body, encoded.
        if(!$body) {
            $body = $this->decodeBody($body['data']);
        }
        // Last try: if we didn't find the body in the last parts, 
        // let's loop for the first parts (text-plain only)
        if(!$body) {
            foreach ($parts  as $part) {
                if($part['body']) {
                    $body = $this->decodeBody($part['body']->data);
                    break;
                }
            }
        }       


        foreach ( $payload->getHeaders() as $header ) {
            switch ($header->name) {
                case 'Date':
                    $date = $header->value;
                    break;
                case 'From':
                    $from = $header->value;
                    break;
                case 'Subject':
                    $subject = $header->value;
                    break;
            }
        }

        if (!$body) {
            $body = $this->decodeBody($result->getPayload()->getBody()->getData());
        }
        $message = array(
            'date' => $date,
            'subject' => $subject,
            'from' => $from,
            'content' => $body
        );



        return $message;

    }

    public function getUserUnreadEmail()
    {
        $client = $this->client;
        $messages = array();

        $email = new \Google_Service_Gmail($client->getClient());
        $emailParam = array(
            'q' => 'is:unread',
            'maxResults' => 5
        );
        $user = 'me';
        $results = $email->users_messages->listUsersMessages($user, $emailParam);
        foreach ($results->getMessages() as $message) {
            $date = $subject = $from = "";
            $content = $email->users_messages->get($user, $message->getId());
            $messagePart = $content->getPayload();

            foreach ( $messagePart->getHeaders() as $header ) {
                switch ($header->name) {
                    case 'Date':
                        $date = $header->value;
                        break;
                    case 'From':
                        $from = $header->value;
                        break;
                    case 'Subject':
                        $subject = $header->value;
                        break;
                }
            }
            $messageContent = array(
                'id' => $content->getId(),
                'date' => $date,
                'subject' => $subject,
                'from' => $from,
                'content' => $content->getSnippet()
            );

            array_push($messages, $messageContent);
        }
        return $messages;

    }

    public function decodeBody($body) {
        $rawData = $body;
        $sanitizedData = strtr($rawData,'-_', '+/');
        $decodedMessage = base64_decode($sanitizedData);
        if(!$decodedMessage){
            $decodedMessage = false;
        }
        return $decodedMessage;
    }
}