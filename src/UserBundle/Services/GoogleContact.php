<?php

namespace UserBundle\Services;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session as Session;
use Symfony\Component\Routing\RouterInterface as Router;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class GoogleContact
 *
 * @package UserBundle\Services
 */
class GoogleContact
{

    protected $client;

    public function __construct($client)
    {
        $this->client = $client;
        return $client;
    }

    public function getUserContact()
    {

        $client = $this->client;
        $accessToken = $client->getClient()->getAccesstoken();
        $url = 'https://www.google.com/m8/feeds/contacts/default/full?access_token='.$accessToken['access_token'];
        $response = $this->CallAPI($url);
        $userContact = new \SimpleXMLElement($response);

        return $userContact;
    }

    public function CallAPI($url) {
        $curl = curl_init();
        // Optional Authentication:
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

}