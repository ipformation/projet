<?php

namespace UserBundle\Services;

use \Google_Client as Google_Client;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session as Session;
use Symfony\Component\Routing\RouterInterface as Router;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class GoogleClient
 *
 * @package UserBundle\Services
 */
class GoogleClient
{

    protected $client;

    protected $accessToken;

    private $router;
    private $session;
    private $container;

    public function __construct(Container $container, Router $routerInterface, Session $session, $googleClientId, $googleClientSecret)
    {

        $client = new \Google_Client();

        $this->client = $client;
        $this->session = $session;
        $this->container = $container;
        $this->router = $routerInterface;

        $client->setClientId($googleClientId);
        $client->setClientSecret($googleClientSecret);

        $client->setRedirectUri($this->router->generate('google_login', array(), Router::ABSOLUTE_URL));
        $client->addScope('email');
        $client->addScope('profile');
        $client->addScope(\Google_Service_Calendar::CALENDAR_READONLY);
        $client->addScope(\Google_Service_Gmail::GMAIL_MODIFY);
        $client->addScope(\Google_Service_Gmail::GMAIL_READONLY);
        $client->addScope(\Google_Service_Gmail::GMAIL_SEND);
        $client->addScope(\Google_Service_Gmail::GMAIL_LABELS);
        $client->addScope("https://www.googleapis.com/auth/contacts.readonly");
        $client->addScope("https://www.google.com/m8/feeds");


        if ($session->get('AccessToken')) {
            $accessToken = $session->get('AccessToken');

            $client->setAccessToken($accessToken);
            if ($client->isAccessTokenExpired()) {
                $client->revokeToken($accessToken);
                $session->remove('AccessToken');
                $this->container->get('security.token_storage')->setToken(null);
                return new RedirectResponse($this->router->generate('fos_user_security_logout'));

            }
        }
        else {

            $this->container->get('security.token_storage')->setToken(null);
            return new RedirectResponse($this->router->generate('fos_user_security_logout'));
        }

        return $client;

    }

    public function getAuthUrl()
    {

        $client = $this->client;
        $authUrl = $client->createAuthUrl();

        return $authUrl;
    }

    public function getUserInfo($code)
    {
        $client = $this->client;

        $token = $client->fetchAccessTokenWithAuthCode($code);
        $client->setAccessToken($token);
        $service = new \Google_Service_Oauth2($client);
        $user = $service->userinfo->get(); //get user info

        return $user;
    }

    /**
     * @return mixed
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @param mixed $accessToken
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return Google_Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param Google_Client $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }
}