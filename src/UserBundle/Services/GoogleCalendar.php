<?php

namespace UserBundle\Services;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session as Session;
use Symfony\Component\Routing\RouterInterface as Router;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class GoogleCalendar
 *
 * @package UserBundle\Services
 */
class GoogleCalendar
{

    protected $client;

    public function __construct($client)
    {
        $this->client = $client;
        return $client;
    }

    public function getUserCalendar()
    {
        $client = $this->client;
        $calendar = new \Google_Service_Calendar($client->getClient());
        $calendarId = 'primary';
        $calendarParams = array(
            'maxResults' => 10,
            'orderBy' => 'startTime',
            'singleEvents' => TRUE,
            'timeMin' => date('c'),
        );

        return $calendar->events->listEvents($calendarId, $calendarParams);
    }

}