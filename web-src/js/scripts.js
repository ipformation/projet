// Declare namespace
var IPPSI = IPPSI || {};

IPPSI.General = function() {
};

IPPSI.General.prototype = {
    init: function() {
        var that = this;
        that.flashNotice();
        that.dataTable();
        that.subMenu();
    },
    dataTable: function() {
        $('#datatable').dataTable(
            {
                "language": {
                    "lengthMenu": "Afficher _MENU_ élements",
                    "zeroRecords": "Aucun enregistrement trouvé",
                    "info": "_PAGE_ sur _PAGES_",
                    "infoEmpty": "Aucun enregistrement disponible",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "search": "Rechercher ",
                    "paginate": {
                        "previous": "précèdent",
                        "next": "suivant",
                    }
                }
            }
        );
        $('#datatable2').dataTable(
            {
                "language": {
                    "lengthMenu": "Afficher _MENU_ élements",
                    "zeroRecords": "Aucun enregistrement trouvé",
                    "info": "_PAGE_ sur _PAGES_",
                    "infoEmpty": "Aucun enregistrement disponible",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "search": "Rechercher ",
                    "paginate": {
                        "previous": "précèdent",
                        "next": "suivant",
                    }
                }
            }
        );
    },
    flashNotice: function () {
        var $flashBag = $('.flashbag');

        if ($flashBag.length > 0) {
            $flashBag.removeClass('hidden');
            setTimeout(function(){
                $flashBag.addClass('hidden');
            }, 3500);
        }

    },
    subMenu: function() {
        $('.submenu').hide();
        $('.has-submenu').on('click', function() {
            $('.submenu').slideToggle( "slow", function() {
                    // Animation complete.
            });
        });

        $('.dropdown').on('click', function () {
            $('.dropdown-menu').removeClass('visible');
            $(this).find('.dropdown-menu').toggleClass('visible');
        });
    }
};

$(document).ready(function() {

    var g = new IPPSI.General();
    g.init();

    // Gestion des formulaires dynamique pour les fournitures
    $('#other_report_extra_transport_style').parent().hide();
    $('#other_report_extra_transport_price').parent().hide();
    $('#other_report_other_supplies').parent().hide();
    $('#other_report_other_supplies_price').parent().hide();
    if ($('#other_report_is_extra_transport_0').is(':checked')){
        $('#other_report_extra_transport_style').parent().show();
        $('#other_report_extra_transport_price').parent().show();
        $("#other_report_extra_transport_style").attr('required',true);
        $("#other_report_extra_transport_price").attr('required',true);
        $('#other_report_other_supplies').parent().hide();
        $('#other_report_other_supplies_price').parent().hide();
    }
    if ($('#other_report_is_extra_transport_1').is(':checked')){
        $('#other_report_other_supplies').parent().show();
        $('#other_report_other_supplies_price').parent().show();
        $("#other_report_other_supplies").attr('required',true);
        $("#other_report_other_supplies_price").attr('required',true);
        $('#other_report_extra_transport_style').parent().hide();
        $('#other_report_extra_transport_price').parent().hide();
    }
    $('#other_report_is_extra_transport_0').on('click', function(){
        $('#other_report_extra_transport_style').parent().show();
        $('#other_report_extra_transport_price').parent().show();
        $("#other_report_extra_transport_style").attr('required',true);
        $("#other_report_extra_transport_price").attr('required',true);
        $('#other_report_other_supplies').parent().hide();
        $('#other_report_other_supplies_price').parent().hide();
    });
    $('#other_report_is_extra_transport_1').on('click', function(){
        $('#other_report_other_supplies').parent().show();
        $('#other_report_other_supplies_price').parent().show();
        $("#other_report_other_supplies").attr('required',true);
        $("#other_report_other_supplies_price").attr('required',true);
        $('#other_report_extra_transport_style').parent().hide();
        $('#other_report_extra_transport_price').parent().hide();
    });
    // Gestion des formulaires dynamique pour les transports
    $('#travel_report_tolls_travel').parent().hide();
    $('#travel_report_is_tolls_travel_1').on('click', function(){
        $('#travel_report_tolls_travel').parent().show();
    });
    $('#travel_report_is_tolls_travel_0').on('click', function(){
        $('#travel_report_tolls_travel').parent().hide();
    });

    $('.dataTables_filter input').attr("placeholder", "Rechercher");


    $('.panel-title').on('click', function() {
        $(this).parent().parent().find('.mail-answer').toggleClass('hidden');
    })
});