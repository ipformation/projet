# Ipssi Project

**Commande pour les droits sur ubuntu** :

```
sudo apt-get install acl
sudo setfacl -R -m u:www-data:rwX -m u:`whoami`:rwX var/cache var/logs
sudo setfacl -dR -m u:www-data:rwx -m u:`whoami`:rwx var/cache var/logs
```

# Commande sql a éxecuter lors de l'installation

CREATE TABLE `sessions` (
    `sess_id` VARBINARY(128) NOT NULL PRIMARY KEY,
    `sess_data` BLOB NOT NULL,
    `sess_time` INTEGER UNSIGNED NOT NULL,
    `sess_lifetime` MEDIUMINT NOT NULL
) COLLATE utf8_bin, ENGINE = InnoDB;

#Connexion ua comtpe google
mail: projet.ip.dev5@gmail.com
mdp: ipformation


**Mise en place du vhost** :

```
cd /etc/apache2/sites-available/
sudo touch projet-ipssi.com.conf
sudo nano projet-ipssi.com.conf

<VirtualHost *:80>
    ServerName projet-ipssi.com
    ServerAlias www.projet-ipssi.com

    DocumentRoot /var/www/html/projet/web
    <Directory /var/www/html/projet/web>
        AllowOverride All
        Order Allow,Deny
        Allow from All
    </Directory>

    # uncomment the following lines if you install assets as symlinks
    # or run into problems when compiling LESS/Sass/CoffeScript assets
    # <Directory /var/www/project>
    #     Options FollowSymlinks
    # </Directory>

    ErrorLog /var/log/apache2/project_error.log
    CustomLog /var/log/apache2/project_access.log combined
</VirtualHost>

sudo a2ensite projet-ipssi.com.conf

sudo nano /etc/hosts
Add :
127.0.0.1 projet-ipssi.com

sudo nano /etc/apache2/apache2.conf
Add :
ServerName localhost

sudo apt-get install php5-dev
sudo apt-get install php5-curl
sudo apt-get install php5-intl
sudo apt-get install php-pear
sudo pecl install mailparse-2.1.6

sudo mkdir /etc/php5/mods-available/
sudo touch /etc/php5/mods-available/mailparse.ini
Add: extension=mailparse.so
sudo php5enmod mailparse

sudo service apache2 restart

composer install
```
