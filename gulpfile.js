

//  Include dependencies
var gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    jshint = require('gulp-jshint'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    plumber = require('gulp-plumber');

/**
 * Paths for sources to be used with Gulp
 */
var paths = {
    css: {
        src: 'web-src/sass/front.scss',
        dest: 'web/css/',
        watch: ['web-src/sass/**/*.scss']
    },
    scripts: {
        src: [
            'web-src/js/vendor/*.js',
            'web-src/js/vendor/bootstrap/*.js'
        ],
        dest: 'web/js/',
        watch: [
            'web-src/js/vendor/*.js',
            'web-src/js/vendor/bootstrap/*.js'
        ]
    },
    js: {
        src: [
            'web-src/js/scripts.js'
        ],
        dest: 'web/js/',
        watch: [
            'web-src/js/scripts.js'
        ]
    },
    img: {
        src: ['web-src/images/*.png', 'web-src/images/*.jpg', 'web-src/images/*.jpeg'],
        dest: 'web/images/'
    }
};

var autoprefixerOptions = {
    browsers: ['last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'],
    cascade: false
};

/**
 * Script task
 */
gulp.task(
    'scripts', function () {
        return gulp.src(paths.scripts.src)
            .pipe(
            plumber(
                {
                    handleError: function (err) {
                        console.log(err);
                        this.emit('end');
                    }
                }))
            .pipe(jshint())
            .pipe(jshint.reporter('jshint-stylish'))
            .pipe(concat('vendors.js'))
            .pipe(uglify())
            .pipe(gulp.dest(paths.scripts.dest));
    });

/**
 * JS task
 */
gulp.task(
    'js', function () {
        return gulp.src(paths.js.src)
            .pipe(
            plumber(
                {
                    handleError: function (err) {
                        console.log(err);
                        this.emit('end');
                    }
                }))
            .pipe(jshint())
            .pipe(jshint.reporter('jshint-stylish'))
            .pipe(concat('scripts.js'))
            .pipe(uglify())
            .pipe(gulp.dest(paths.js.dest));
    });

/**
 * Styles tasks
 */
gulp.task(
    'styles', function () {
        return gulp.src(paths.css.src)
            .pipe(sass.sync().on('error', sass.logError))
            .pipe(autoprefixer(autoprefixerOptions))
            .pipe(gulp.dest(paths.css.dest));
    });

gulp.task(
    'watch', function () {
        gulp.watch(paths.css.watch.concat(paths.css.src), ['styles']);
        gulp.watch(paths.scripts.watch, ['scripts']);
        gulp.watch(paths.js.watch, ['js']);
    });

/**
 * Image task
 */

gulp.task(
    'img', function () {
        return gulp.src(paths.img.src)
            .pipe(imagemin({
                progressive: true,
                use: [pngquant()]
            }))
            .pipe(gulp.dest(paths.img.dest));
    });

gulp.task(
    'default', [
        'js',
        'scripts',
        'styles'
    ]);

